package com.lzp.java.concurrent.collection.copyonwritearraylist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author lzp
 * @Description: CopyOnWriteArrayList迭代的问题
 * 1.ArrayList迭代时，如果对列表进行结构修改，会触发Fail-Fast机制，抛出并发修改异常。
 * 2.CopyOnWriteArrayList在迭代时，可以对列表进行修改，但是迭代的结果集仍是修改之前的。
 * @date 2021/2/28
 */
public class CopyOnWriteArrayListIterator {
    public static void main(String[] args) {
        // List<String> list = new ArrayList<>();
        List<String> list = new CopyOnWriteArrayList<>();
        list.add("1");
        list.add("3");
        list.add("5");

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(list);
            String next = iterator.next();
            System.out.println(next);

            if ("3".equals(next)) {
                list.add("100");
            }
        }
    }
}
