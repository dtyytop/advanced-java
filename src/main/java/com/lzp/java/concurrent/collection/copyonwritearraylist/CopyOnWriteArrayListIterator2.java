package com.lzp.java.concurrent.collection.copyonwritearraylist;

import com.sun.javaws.IconUtil;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author lzp
 * @Description: CopyOnWriteArrayList迭代的问题
 * 1.数据取决于迭代器生成的时候，而不会在迭代后更新。
 *
 * @date 2021/2/28
 */
public class CopyOnWriteArrayListIterator2 {
    public static void main(String[] args) {
        // List<String> list = new ArrayList<>();
        List<String> list = new CopyOnWriteArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        System.out.println(list);
        Iterator<String> iterator1 = list.iterator();
        list.add("5");
        iterator1.forEachRemaining(System.out::println);
        Iterator<String> iterator2 = list.iterator();
        iterator2.forEachRemaining(System.out::println);
    }
}
