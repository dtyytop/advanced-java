package com.lzp.java.concurrent.flowcontrol.countdownlatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author lzp
 * @Description: 多等一，例如运动员等待裁判发令枪。
 * @date 2021/3/5
 */
public class NAwaitOne {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ExecutorService pool = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++) {
            final int num = i + 1;
            pool.submit(() -> {
                try {
                    System.out.println("运动员" + num + "准备就绪~");
                    latch.await();
                    System.out.println("运动员" + num + "开始跑步~");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        System.out.println("裁判开始准备工作，等待5秒");
        TimeUnit.SECONDS.sleep(5);
        System.out.println("裁判准备完成，开始跑步");
        latch.countDown();

    }
}
