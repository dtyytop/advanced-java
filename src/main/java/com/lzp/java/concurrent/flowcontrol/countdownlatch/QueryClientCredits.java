package com.lzp.java.concurrent.flowcontrol.countdownlatch;

import java.util.concurrent.*;

/**
 * @author lzp
 * @Description: 完成上线前的准备，再发布
 * @date 2021/3/5
 */
public class QueryClientCredits {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(3);
        ExecutorService pool = new ThreadPoolExecutor(
                3, 3, 0, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());
        for (int i = 0; i < 3; i++) {
            final int zxId = i + 1;
            pool.submit(() ->{
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 1000));
                    System.out.println("已经查到" + zxId + "资信");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    latch.countDown();
                }
            });
        }
        System.out.println("等待请求资信详情...");
        latch.await();
        System.out.println("已经查到所有资信，下一步进行决策。");
    }
}
