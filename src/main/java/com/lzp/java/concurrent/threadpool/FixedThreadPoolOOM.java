package com.lzp.java.concurrent.threadpool;

import com.lzp.mybatis.custom.utils.Executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * FixedThreadPool造成请求堆积，导致OOM
 *
 * @author lzp
 * @date 2020/03/15
 */
public class FixedThreadPoolOOM {
    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(1);
        for(int i = 0; i < Integer.MAX_VALUE; i++) {
            System.out.println(i);
            pool.execute(new Task());
        }
    }
}

class Task implements Runnable{

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
