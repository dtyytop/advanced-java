package com.lzp.java.concurrent.cas;

/**
 * cas等价代码
 *
 * @author lzp
 * @date 2020/04/28
 */
public class SimulateCAS {
    private volatile int value;

    public synchronized int compareAndSwap(int expectedValue, int updateValue) {
        int currentValue = value;
        if (currentValue == expectedValue) {
            value = updateValue;
        }
        return currentValue;
    }
}
