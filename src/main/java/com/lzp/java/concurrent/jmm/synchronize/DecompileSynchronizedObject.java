package com.lzp.java.concurrent.jmm.synchronize;

/**
 * 反编译字节码，查看monitor指令
 */
public class DecompileSynchronizedObject {
    private Object object = new Object();

    public void insert(Thread thread) {
        synchronized (object) {
        }
    }
}
