package com.lzp.java.concurrent.jmm.synchronize;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * synchronized加锁释放锁的等价lock代码
 */
public class SynchronizedAndLock {
    Lock lock = new ReentrantLock();

    public synchronized void method1(){
        System.out.println("synchronized锁");
    }

    public void method2 (){
        lock.lock();
        try {
            System.out.println("lock形式锁");
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        SynchronizedAndLock synchronizedAndLock = new SynchronizedAndLock();
        synchronizedAndLock.method1();
        synchronizedAndLock.method2();
    }
}
