package com.lzp.java.concurrent.jmm.synchronize;

/**
 * 两个线程同时a++，结果比预计小
 */
public class DisappearRequest implements Runnable {

    private static int count = 0;

    private static DisappearRequest disappearRequest = new DisappearRequest();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(disappearRequest);
        Thread t2 = new Thread(disappearRequest);
        t1.start();
        t2.start();
        /* 使用join等待线程结束*/
        t1.join();
        t2.join();
        System.out.println(count);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            count++;
        }
    }
}
