package com.lzp.java.concurrent.jmm;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * volatile对a++无效，不具备原子性--只能保证从主存读取时读到的是最新的值
 *
 * @author lzp
 * @date 2020/02/24
 */
public class NoVolatileAPlusPlus implements Runnable {
    private volatile int index;
    private AtomicInteger realIndex = new AtomicInteger();

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            index++;
            realIndex.incrementAndGet();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Runnable task = new NoVolatileAPlusPlus();
        Thread thread1 = new Thread(task);
        Thread thread2 = new Thread(task);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("运行结果：" + ((NoVolatileAPlusPlus) task).index);
        System.out.println("实际运行次数：" + ((NoVolatileAPlusPlus) task).realIndex);
    }
}
