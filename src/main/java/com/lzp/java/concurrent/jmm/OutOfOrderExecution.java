package com.lzp.java.concurrent.jmm;

import java.util.concurrent.CountDownLatch;

/**
 * 测试Java程序的重排序
 * 1. 可以出现四种组合的结果
 * 2. 不排除是可见性导致的表面重排序
 *
 * @author lzp
 * @date 2020/02/22
 */
public class OutOfOrderExecution {
    private static int x = 0, y = 0;
    private static int a = 0, b = 0;

    public static void main(String[] args) throws InterruptedException {
        int i = 0;
        for (; ; ) {
            i++;
            // 初始化
            x = 0;
            y = 0;
            a = 0;
            b = 0;

            // 闭锁--同步计数器
            CountDownLatch latch = new CountDownLatch(3);

            Thread thread1 = new Thread(() -> {
                // 同步计数器减1
                latch.countDown();
                try {
                    // 等待闭锁信号
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                a = 1;
                x = b;
            });
            Thread thread2 = new Thread(() -> {
                latch.countDown();
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                b = 1;
                y = a;
            });
            thread1.start();
            thread2.start();
            // 闭锁计数器为0，所有线程一同出发
            latch.countDown();
            thread1.join();
            thread2.join();

            String result = "第" + i + "次，x = " + x + ",y = " + y;
            if (x == 0 && y == 0) {
                System.out.println(result);
                break;
            } else {
                System.out.println(result);
            }
        }
    }
}
