package com.lzp.java.concurrent.jmm.synchronize;

/**
 * 类锁-- 同步代码块
 */
public class SynchronizedClassClass implements Runnable {
    private static SynchronizedClassClass instance1 = new SynchronizedClassClass();
    private static SynchronizedClassClass instance2 = new SynchronizedClassClass();

    @Override
    public void run() {
        method();
    }

    private void method() {
        // SynchronizedClassClass.class/ this对比
        synchronized (SynchronizedClassClass.class) {
            System.out.println("当前线程" + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("线程结束" + Thread.currentThread().getName());
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(instance1);
        Thread t2 = new Thread(instance2);
        t1.start();
        t2.start();
        /* 死循环等待线程结束*/
        while (t1.isAlive() || t2.isAlive()) {
        }
    }
}
