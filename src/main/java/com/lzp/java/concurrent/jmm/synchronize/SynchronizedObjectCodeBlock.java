package com.lzp.java.concurrent.jmm.synchronize;

/**
 * 使用对象锁--代码块形式 锁对象使用this
 */
public class SynchronizedObjectCodeBlock implements Runnable {
    private static SynchronizedObjectCodeBlock instance = new SynchronizedObjectCodeBlock();

    public static void main(String[] args) {
        Thread t1 = new Thread(instance);
        Thread t2 = new Thread(instance);
        t1.start();
        t2.start();
        /* 死循环等待线程结束*/
        while (t1.isAlive() || t2.isAlive()) {

        }
    }

    @Override
    public void run() {
        synchronized (this) {
            System.out.println("当前线程" + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("线程结束" + Thread.currentThread().getName());
        }
    }
}
