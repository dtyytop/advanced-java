package com.lzp.java.concurrent.jmm.synchronize;

/**
 * 方法抛异常后，会释放锁。
 * 一旦抛出异常，第二个线程立刻进入同步方法，意味着锁释放。
 */
public class SynchronizedException implements Runnable {

    private static SynchronizedException instance = new SynchronizedException();

    @Override
    public void run() {
        if (Thread.currentThread().getName().equals("Thread-0")) {
            method1();
        } else {
            method2();
        }
    }

    public synchronized void method1() {
        System.out.println("同步方法1，当前线程" + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
            throw new RuntimeException("同步方法抛出运行异常");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程结束" + Thread.currentThread().getName());
    }

    public synchronized void method2() {
        System.out.println("同步方法2，当前线程" + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程结束" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(instance);
        Thread t2 = new Thread(instance);
        t1.start();
        t2.start();
        /* 死循环等待线程结束*/
        while (t1.isAlive() || t2.isAlive()) {
        }
    }
}
