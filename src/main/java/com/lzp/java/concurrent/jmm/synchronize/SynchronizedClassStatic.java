package com.lzp.java.concurrent.jmm.synchronize;

/**
 * 类锁--静态方法形式
 */
public class SynchronizedClassStatic implements Runnable {
    private static SynchronizedClassStatic instance1 = new SynchronizedClassStatic();
    private static SynchronizedClassStatic instance2 = new SynchronizedClassStatic();

    @Override
    public void run() {
        method();
    }

    public static synchronized void method() {
        System.out.println("当前线程" + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程结束" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(instance1);
        Thread t2 = new Thread(instance2);
        t1.start();
        t2.start();
        /* 死循环等待线程结束*/
        while (t1.isAlive() || t2.isAlive()) {
        }
    }
}
