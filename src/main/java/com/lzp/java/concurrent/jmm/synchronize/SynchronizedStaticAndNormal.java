package com.lzp.java.concurrent.jmm.synchronize;

/**
 * 同时访问同步和非同步方法
 */
public class SynchronizedStaticAndNormal implements Runnable {
    private static SynchronizedStaticAndNormal instance = new SynchronizedStaticAndNormal();

    @Override
    public void run() {
        if (Thread.currentThread().getName().equals("Thread-0")) {
            method1();
        } else {
            method2();
        }
    }

    public static synchronized void method1() {
        System.out.println("静态同步方法，当前线程" + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程结束" + Thread.currentThread().getName());
    }

    public synchronized void method2() {
        System.out.println("非静态同步方法，当前线程" + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程结束" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(instance);
        Thread t2 = new Thread(instance);
        t1.start();
        t2.start();
        /* 死循环等待线程结束*/
        while (t1.isAlive() || t2.isAlive()) {
        }
    }
}
