package com.lzp.java.concurrent.jmm.synchronize;

/**
 * 反编译字节码，查看monitor指令
 */
public class DecompileSynchronizedMethod implements Runnable {
    public synchronized void insert() {
    }

    @Override
    public void run() {
        insert();
    }
}
