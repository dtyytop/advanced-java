package com.lzp.java.concurrent.jmm.synchronize;

/**
 * 对象锁--普通方法
 */
public class SynchronizedObjectMethod implements Runnable {
    private static SynchronizedObjectCodeBlock instance = new SynchronizedObjectCodeBlock();

    public static void main(String[] args) {
        Thread t1 = new Thread(instance);
        Thread t2 = new Thread(instance);
        t1.start();
        t2.start();
        /* 死循环等待线程结束*/
        while (t1.isAlive() || t2.isAlive()) {

        }
    }

    @Override
    public void run() {
        method();
    }

    public synchronized void method() {
        System.out.println("当前线程" + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程结束" + Thread.currentThread().getName());
    }
}
