package com.lzp.java.concurrent.threadlocal;

/**
 * 泛型使用不当，引发NPE
 *
 * @author lzp
 * @date 2020/03/30
 */
public class WrongWayNPE {
    private static ThreadLocal<Integer> integerThreadLocal = new ThreadLocal<>();

    public static void main(String[] args) {
        System.out.println(integerThreadLocal.get());
        System.out.println(getThreadLocal());
    }

    static int getThreadLocal() {
        return integerThreadLocal.get();
    }
}
