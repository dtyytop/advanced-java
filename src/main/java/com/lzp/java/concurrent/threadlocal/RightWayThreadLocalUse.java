package com.lzp.java.concurrent.threadlocal;

/**
 * 用于存储线程中业务信息，避免请求时层层传递参数
 * <p>
 * 假设，user信息需要从service1,传递到service10，会导致代码冗余且不易维护
 * <p>
 * 其他思路：
 * 利用共享的Map：可以用static的ConcurrentHashMap，把当前线程的ID作为key，把user作为value来保存，这样可以做到线程间的隔离，但是依然有性能影响。
 *
 * @author lzp
 * @date 2020/03/29
 */
public class RightWayThreadLocalUse {
    public static void main(String[] args) {
        new ServiceImpl1().service(new UserInfo("lzp", "1234567890"));
    }
}

/**
 * 接收数据
 */
class ServiceImpl1 {
    public void service(UserInfo userInfo) {
        UserInfoHolder.holder.set(userInfo);
        new ServiceImpl2().service();
    }
}

/**
 * 处理数据1
 */
class ServiceImpl2 {
    public void service() {
        System.out.println("客户名：" + UserInfoHolder.holder.get().cltNam);
        new ServiceImpl3().service();
    }
}

/**
 * 处理数据2
 */
class ServiceImpl3 {
    public void service() {
        System.out.println("客户号：" + UserInfoHolder.holder.get().cltNbr);
        // 此时使用完ThreadLocal，回收该ThreadLocal
        UserInfoHolder.holder.remove();
    }
}

class UserInfoHolder {
    public static ThreadLocal<UserInfo> holder = new ThreadLocal<>();
}

class UserInfo {
    String cltNam;
    String cltNbr;

    public UserInfo(String cltNam, String cltNbr) {
        this.cltNam = cltNam;
        this.cltNbr = cltNbr;
    }
}
