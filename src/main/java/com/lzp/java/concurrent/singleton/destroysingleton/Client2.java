package com.lzp.java.concurrent.singleton.destroysingleton;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 序列化外部调用类
 *
 * @author lzp
 * @date 2020/03/14
 */
public class Client2 {
    public static void main(String[] args) throws Exception {
        // 使用全局访问方法创建实例
        SerializeSingleton instance = SerializeSingleton.getInstance();

        // 写出对象到项目目录下singleton.txt文件
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("singleton.txt"));
        oos.writeObject(instance);
        // 读入对象
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("singleton.txt"));
        SerializeSingleton newInstance = (SerializeSingleton) ois.readObject();

        System.out.println(instance);
        System.out.println(newInstance);
        System.out.println(instance == newInstance);
    }
}
