package com.lzp.java.concurrent.singleton;

/**
 * 使用枚举实现单例模式
 *
 * @author lzp
 * @date 2020/03/01
 */
public enum EnumSingleton {
    // INSTANCE {
    //     @Override
    //     protected void print() {
    //         System.out.println("enum singleton");
    //     }
    // };
    INSTANCE;
    // protected abstract void print();

    public static EnumSingleton getInstance() {
        return INSTANCE;
    }

    public static void main(String[] args) {
        EnumSingleton instance = EnumSingleton.getInstance();
    }
}
