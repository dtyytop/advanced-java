package com.lzp.java.concurrent.singleton.destroysingleton;

import java.io.*;

/**
 * 序列化单例模式
 *
 * @author lzp
 * @date 2020/02/26
 */
public class SerializeSingleton implements Serializable {
    private final static SerializeSingleton instance = new SerializeSingleton();

    private SerializeSingleton() {
    }

    public static SerializeSingleton getInstance() {
        return instance;
    }

    /**
     * 避免序列化破坏单例模式
     *
     * @return
     */
    private Object readResolve() {
        return instance;
    }

}
