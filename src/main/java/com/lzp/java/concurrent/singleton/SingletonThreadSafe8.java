package com.lzp.java.concurrent.singleton;

/**
 * 枚举--线程安全--推荐使用
 *
 * @author lzp
 * @date 2020/02/27
 */
public enum SingletonThreadSafe8 {
    INSTANCE;

}
