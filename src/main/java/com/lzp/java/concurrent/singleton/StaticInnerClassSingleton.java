package com.lzp.java.concurrent.singleton;

/**
 * 静态内部类--线程安全--推荐使用
 * 1. 属于懒汉，外部类加载的时候不会初始化内部
 * 2.
 *
 * @author lzp
 * @date 2020/02/27
 */
public class StaticInnerClassSingleton {

    private StaticInnerClassSingleton() {

    }

    private static class InnerClass {
        private static final StaticInnerClassSingleton INSTANCE = new StaticInnerClassSingleton();
    }

    public static StaticInnerClassSingleton getInstance() {
        return InnerClass.INSTANCE;
    }
}
