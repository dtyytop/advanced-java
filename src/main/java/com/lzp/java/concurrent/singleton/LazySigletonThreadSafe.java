package com.lzp.java.concurrent.singleton;

/**
 * 懒汉式--线程安全--不推荐使用（效率）
 *
 * @author lzp
 * @date 2020/02/27
 */
public class LazySigletonThreadSafe {
    private static LazySigletonThreadSafe instance;

    private LazySigletonThreadSafe() {

    }

    public synchronized static LazySigletonThreadSafe getInstance() {
        if (instance == null) {
            instance = new LazySigletonThreadSafe();
        }
        return instance;
    }

    public static LazySigletonThreadSafe getInstance1() {
        synchronized (LazySigletonThreadSafe.class) {
            if (instance == null) {
                instance = new LazySigletonThreadSafe();
            }
        }
        return instance;
    }
}
