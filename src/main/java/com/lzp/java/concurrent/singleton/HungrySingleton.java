package com.lzp.java.concurrent.singleton;

/**
 * 饿汉式（静态常量实现）--线程安全--推荐使用
 *
 * @author lzp
 * @date 2020/02/26
 */
public class HungrySingleton {
    private final static HungrySingleton instance = new HungrySingleton();

    private HungrySingleton() {
    }

    public static HungrySingleton getInstance() {
        return instance;
    }
}
