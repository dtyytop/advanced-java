package com.lzp.java.concurrent.singleton.destroysingleton;

import java.lang.reflect.Constructor;

/**
 * 反射攻击单例模式,获取多个对象
 *
 * @author lzp
 * @date 2020/02/26
 */
public class ReflectSingleton {
    private final static ReflectSingleton instance = new ReflectSingleton();

    private ReflectSingleton() {
        if (instance != null) {
            throw new RuntimeException("禁止反射调用创建多个实例");
        }
    }

    public static ReflectSingleton getInstance() {
        return instance;
    }
}
