package com.lzp.java.concurrent.singleton;

/**
 * 饿汉式（静态代码块初始化)--线程安全--推荐使用
 *
 * @author lzp
 * @date 2020/02/27
 */
public class HungrySingleton2 {
    private final static HungrySingleton2 instance;

    static {
        instance = new HungrySingleton2();
    }

    private HungrySingleton2() {

    }

    public static HungrySingleton2 getInstance() {
        return instance;
    }
}
