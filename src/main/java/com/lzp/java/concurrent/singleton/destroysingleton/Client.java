package com.lzp.java.concurrent.singleton.destroysingleton;

import java.lang.reflect.Constructor;

/**
 * 反射外部调用类
 *
 * @author lzp
 * @date 2020/03/14
 */
public class Client {
    public static void main(String[] args) throws Exception {
        // 通过全局访问方法创建实例
        ReflectSingleton instance = ReflectSingleton.getInstance();
        // 通过反射创建实例
        Constructor constructor = ReflectSingleton.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        ReflectSingleton newInstance = (ReflectSingleton) constructor.newInstance();

        System.out.println(instance);
        System.out.println(newInstance);
        System.out.println(instance == newInstance);
    }
}
