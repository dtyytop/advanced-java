package com.lzp.java.concurrent.singleton;

/**
 * 懒汉式--线程不安全
 *
 * @author lzp
 * @date 2020/02/27
 */
public class LazySingleton {
    private static LazySingleton instance;

    private LazySingleton() {

    }

    public static LazySingleton getInstance() {
        // 竞态条件：先检查再执行
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}
