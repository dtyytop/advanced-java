package com.lzp.java.concurrent.future;

import java.util.concurrent.*;

/**
 * @author lzp
 * @Description: 测试Future的使用
 * @date 2021/3/9
 */
public class FutureDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        Callable<Integer> callable = () -> {
            TimeUnit.SECONDS.sleep(3);
            return (int) (Math.random() * 10);
        };
        Future<Integer> future = pool.submit(callable);
        System.out.println(future.get());
    }
}
