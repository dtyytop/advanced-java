package com.lzp.java.concurrent.threadcore.createthreads.wrongways;

/**
 * 使用匿名内部类方式创建线程-- 仅为表象
 *
 * @author lzp
 * @date 2020/01/30
 */
public class AnonymousInnerClassStyle {
    public static void main(String[] args) {
        new Thread() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        }.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        }).start();
    }
}
