package com.lzp.java.concurrent.threadcore.uncaughtexception;

/**
 * 使用自定义未捕获异常处理器
 *
 * @author lzp
 * @date 2020/02/12
 */
public class UseMyUncaughtExceptionHandler implements Runnable {
    public static void main(String[] args) throws InterruptedException {
        Thread.setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler("异常捕获器"));
        new Thread(new UseMyUncaughtExceptionHandler(), "Thread-1").start();
        Thread.sleep(300);
        new Thread(new UseMyUncaughtExceptionHandler(), "Thread-2").start();
    }

    @Override
    public void run() {
        throw new RuntimeException();
    }
}
