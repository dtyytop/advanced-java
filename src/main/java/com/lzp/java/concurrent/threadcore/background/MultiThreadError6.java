package com.lzp.java.concurrent.threadcore.background;

import java.util.HashMap;
import java.util.Map;

/**
 * 在构造器中创造新的线程
 *
 * 初始化的工作在另外一个线程中，可能没有执行完毕。这会导致在不同的时间获取对象结果不同
 *
 * @author lzp
 * @date 2020/02/18
 */
public class MultiThreadError6 {

    private Map<String, String> states;

    public MultiThreadError6() {
        // 在另一个线程中初始化
        new Thread(new Runnable() {
            @Override
            public void run() {
                states = new HashMap<>();
                states.put("1", "星期一");
                states.put("2", "星期二");
                states.put("3", "星期三");
            }
        }).start();
    }

    public Map<String, String> getStates() {
        return states;
    }

    public static void main(String[] args) {
        MultiThreadError6 multiThreadError6 = new MultiThreadError6();
        // 延时可以打印正确结果
        // try {
        //     Thread.sleep(100);
        // } catch (InterruptedException e) {
        //     e.printStackTrace();
        // }
        System.out.println(multiThreadError6.getStates().get("1"));
    }
}
