package com.lzp.java.concurrent.threadcore.background;

/**
 * @Auther: lzp
 * @Date: 2019/12/29 16:57
 * @Description: 创建100个线程，监测线程数量变化
 */
public class Create100Threads {
    public static void main(String[] args) {
        for(int i = 0; i < 200; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
