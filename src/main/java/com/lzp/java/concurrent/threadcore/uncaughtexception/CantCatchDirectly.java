package com.lzp.java.concurrent.threadcore.uncaughtexception;

/**
 * 1. 不加try catch抛出4个异常，打印出堆栈信息
 * 2. main方法加了try catch,期望捕获到第一个线程的异常，线程234不应该运行，希望看到打印出Caught Exception
 * 3. 执行时发现，根本没有Caught Exception，线程234依然运行并且抛出异常
 * <p>
 * 说明线程的异常不能用传统方法捕获
 */
public class CantCatchDirectly implements Runnable {

    public static void main(String[] args) throws InterruptedException {
        // trycatch只能捕获对应线程内的异常，当前trycatch无法捕获子线程的异常
        try {
            new Thread(new CantCatchDirectly(), "Thread-1").start();
            Thread.sleep(300);
            new Thread(new CantCatchDirectly(), "Thread-2").start();
        } catch (RuntimeException e) {
            // 执行不到的语句
            System.out.println("Caught Exception.");
        }
    }

    @Override
    public void run() {
        // try {
        throw new RuntimeException();
        // } catch (RuntimeException e) {
        //     System.out.println("捕获异常.");
        // }
    }
}
