package com.lzp.java.concurrent.threadcore.background;

/**
 * 演示死锁
 *
 * @author lzp
 * @date 2020/02/18
 */
public class MultiThreadError2 {
    /**
     * 锁
     */
    private static Object resourceA = new Object();
    private static Object resourceB = new Object();

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            synchronized (resourceA) {
                System.out.println(Thread.currentThread().getName() + "获取到resourceA锁");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (resourceB) {
                    System.out.println(Thread.currentThread().getName() + "获取到resourceB锁");
                }
            }
        });
        Thread thread2 = new Thread(() -> {
            synchronized (resourceB) {
                System.out.println(Thread.currentThread().getName() + "获取到resourceB锁");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (resourceA) {
                    System.out.println(Thread.currentThread().getName() + "获取到resourceA锁");
                }
            }
        });
        thread1.start();
        thread2.start();
    }
}
