package com.lzp.java.concurrent.threadcore.stopthread;

/**
 * 当try..catch代码块在while中时，只有首次迭代中断，中断后清除中断标志，代码继续执行
 *
 * @author lzp
 * @date 2020/01/31
 */
public class CantInterruptTrycatchInWhile {
    public static void main(String[] args) throws InterruptedException {
        Runnable task = () -> {
          int num = 0;
          while (num <= 300) {
              System.out.println(num);
              num++;
              try {
                  Thread.sleep(10);
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
          }
        };
        Thread thread = new Thread(task);
        thread.start();
        Thread.sleep(500);
        thread.interrupt();
    }
}
