package com.lzp.java.concurrent.threadcore.startthread;

/**
 * 使用start和使用run方法区别
 *
 * @author lzp
 * @date 2020/01/31
 */
public class StartAndRunMethod {
    public static void main(String[] args) {
        Runnable task = () -> {
            System.out.println(Thread.currentThread().getName());
        };
        task.run();
        // new Thread(task).run();
        new Thread(task).start();
    }
}
