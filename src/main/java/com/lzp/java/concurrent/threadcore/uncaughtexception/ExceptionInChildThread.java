package com.lzp.java.concurrent.threadcore.uncaughtexception;

/**
 * 子线程抛出异常，很难在日志中察觉
 */
public class ExceptionInChildThread implements Runnable {

    public static void main(String[] args) {
        new Thread(new ExceptionInChildThread()).start();
        for (int i = 0; i < 1000; i++) {
            System.out.println(i);
        }
    }

    @Override
    public void run() {
        throw new RuntimeException();
    }
}