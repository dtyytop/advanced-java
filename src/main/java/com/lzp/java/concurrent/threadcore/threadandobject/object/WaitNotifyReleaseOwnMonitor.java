package com.lzp.java.concurrent.threadcore.threadandobject.object;

/**
 * wait方法只释放当前的那把锁
 *
 * @author lzp
 * @date 2020/02/05
 */
public class WaitNotifyReleaseOwnMonitor {
    // 定义两把锁
    private static final Object resourceA = new Object();
    private static final Object resourceB = new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            synchronized (resourceA) {
                System.out.println(Thread.currentThread().getName() + "获取到resourceA的monitor锁");
                synchronized (resourceB) {
                    System.out.println(Thread.currentThread().getName() + "获取到resourceB的monitor锁");
                    try {
                        System.out.println(Thread.currentThread().getName() + "释放resourceA的monitor锁");
                        resourceA.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        Thread thread2 = new Thread(() -> {
            synchronized (resourceA) {
                System.out.println(Thread.currentThread().getName() + "获取到resourceA的monitor锁");
                System.out.println(Thread.currentThread().getName() + "尝试获取到resourceB的monitor锁");
                synchronized (resourceB) {
                    System.out.println(Thread.currentThread().getName() + "获取到resourceB的monitor锁");
                }
            }
        });
        thread1.start();
        Thread.sleep(1000);
        thread2.start();
    }
}
