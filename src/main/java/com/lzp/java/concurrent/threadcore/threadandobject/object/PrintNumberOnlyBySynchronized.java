package com.lzp.java.concurrent.threadcore.threadandobject.object;

/**
 * 交替打印0-100奇偶数--使用synchronized实现
 * 1. 存在的问题说明：
 * synchronized代码在while迭代中，每迭代一次，两个线程都会竞争锁。
 * 假设在count为奇数时，偶数线程竞争到了锁，必然是不满足条件的，此次竞争无效。
 * 2. 使用了位运算代替取余操作提高性能
 * @author lzp
 * @date 2020/02/08
 */
public class PrintNumberOnlyBySynchronized {
    private static int count;
    private static final Object lock = new Object();

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // 此处while无法放到synchronized中去
                // 由于线程在while内开始阻塞进入到BLOCKED，因此count<100仍然可以打印出100.
                while (count <= 100) {
                    synchronized (lock) {
                        // 注：位运算快于取余操作
                        if ((count & 1) == 0) {
                            System.out.println(Thread.currentThread().getName() + "打印" + count++);
                        }
                    }
                }
            }
        }, "偶数线程").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (count < 100) {
                    synchronized (lock) {
                        if ((count & 1) == 1) {
                            System.out.println(Thread.currentThread().getName() + "打印" + count++);
                        }
                    }
                }
            }
        }, "奇数线程").start();
    }
}
