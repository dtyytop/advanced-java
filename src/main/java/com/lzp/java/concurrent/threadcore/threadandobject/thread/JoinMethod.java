package com.lzp.java.concurrent.threadcore.threadandobject.thread;

import java.util.concurrent.TimeUnit;

/**
 * join基本用法--等待其他线程结束
 *
 * @author lzp
 * @date 2020/02/09
 */
public class JoinMethod {
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(5);
                System.out.println(Thread.currentThread().getName() + "运行结束");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread thread2 = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(10);
                System.out.println(Thread.currentThread().getName() + "运行结束");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread1.start();
        thread2.start();
        thread1.join(); // 抛出中断异常
        thread2.join();
        System.out.println("所有线程都运行结束");
    }
}
