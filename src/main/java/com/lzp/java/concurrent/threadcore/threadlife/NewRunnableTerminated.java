package com.lzp.java.concurrent.threadcore.threadlife;

/**
 * 输出如题目三种线程状态
 *
 * @author lzp
 * @date 2020/02/03
 */
public class NewRunnableTerminated implements Runnable{
    @Override
    public void run() {
        for(int i = 0; i < 1000; i++) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        Thread thread = new Thread(new NewRunnableTerminated());
        // 打印NEW
        System.out.println(thread.getState());
        thread.start();
        // 打印RUNNABLE
        System.out.println(thread.getState());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 打印TERMINATED
        System.out.println(thread.getState());
    }
}
