package com.lzp.java.concurrent.threadcore.threadandobject.thread;

/**
 * 中断调用join语句的线程
 * 1. 中断的是调用jion的语句的调用线程，而非被调线程
 * 2. 调用线程在jion期间是WAITING状态
 *
 * @author lzp
 * @date 2020/02/09
 */
public class JoinInterrupt {
    public static void main(String[] args) {
        Thread mainThread = Thread.currentThread();
        Thread subThread = new Thread(() -> {
            try {
                Thread.sleep(1000);
                System.out.println("main线程状态：" + mainThread.getState());
                System.out.println("此时中断main线程");
                mainThread.interrupt();
                Thread.sleep(5000);
                System.out.println("子线程执行结束");
            } catch (InterruptedException e) {
                System.out.println("子线程被中断");
                e.printStackTrace();
            }
        });
        subThread.start();
        System.out.println("等待子线程运行完毕");
        try {
            subThread.join();
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + "线程被中断");
            subThread.interrupt();
        }

    }
}
