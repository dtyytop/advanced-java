package com.lzp.java.concurrent.threadcore.threadandobject.object;

/**
 * WAITING状态线程注意点
 * 1. WAITING状态仍然可以响应中断
 * 2.
 * @author lzp
 * @date 2020/02/06
 */
public class WaitingStatusToInterrupt {
    private static Object resourceA = new Object();

    static class Thread1 extends Thread {
        @Override
        public void run() {
            synchronized (resourceA) {
                System.out.println(Thread.currentThread().getName() + "--获取到resourceA锁");
                try {
                    System.out.println(Thread.currentThread().getName() + "--释放resourceA锁");
                    resourceA.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "--再次获取到resourceA锁");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class Thread2 extends Thread {
        @Override
        public void run() {
            synchronized (resourceA) {
                System.out.println(Thread.currentThread().getName() + "--获取到resourceA锁");
                resourceA.notify();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread1 thread1 = new Thread1();
        thread1.start();
        Thread.sleep(1000);
        System.out.println("----------------------");
        System.out.println(thread1.getName() + "--" + thread1.getState());
        thread1.interrupt();
        System.out.println("----------------------");
        Thread2 thread2 = new Thread2();
        thread2.start();
        Thread.sleep(500);
        System.out.println(thread2.getName() + "--" + thread2.getState());
    }
}
