package com.lzp.java.concurrent.threadcore.createthreads.wrongways;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 使用定时器方式创建线程
 *
 * @author lzp
 * @date 2020/01/30
 */
public class TimerStyle {
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        },100,100);
    }
}
