package com.lzp.java.concurrent.threadcore.createthreads;

/**
 * 同时使用Runnable和Thread两种方式
 */
public class BothRunnableAndThread {
    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("使用Runnable方式创建线程");
            }
        }) {
            @Override
            public void run() {
                System.out.println("使用Thread方式创建线程");
            }
        };
        thread.start();
    }
}
