package com.lzp.java.concurrent.threadcore.threadandobject.thread;

/**
 * sleep方法不释放synchronized monitor锁
 *
 * @author lzp
 * @date 2020/02/09
 */
public class SleepDontReleaseMonitor implements Runnable {

    public static void main(String[] args) {
        Runnable task = new SleepDontReleaseMonitor();
        new Thread(task).start();
        new Thread(task).start();
    }

    @Override
    public void run() {
        syn();
    }

    private synchronized void syn() {
        System.out.println(Thread.currentThread().getName() + "获取到了monitor锁");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "退出了同步代码块");
    }
}
