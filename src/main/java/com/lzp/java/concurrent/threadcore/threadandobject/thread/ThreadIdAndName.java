package com.lzp.java.concurrent.threadcore.threadandobject.thread;

import java.util.List;
import java.util.stream.IntStream;

/**
 * 线程ID 线程ID从1开始，同时会创建很多守护线程
 *
 * @author lzp
 * @date 2020/02/10
 */
public class ThreadIdAndName {
    public static void main(String[] args) {
        // 线程ID
        Thread thread = new Thread();
        System.out.println(Thread.currentThread().getId());
        System.out.println(thread.getId());

        // 打印线程名称
        IntStream.range(0, 5).boxed().map(i -> new Thread(() -> System.out.println(Thread.currentThread().getName()))).forEach(Thread::start);

        // 线程的线程组
        Thread thread1 = new Thread("thread1");
        ThreadGroup threadGroup = new ThreadGroup("threadGroup");
        Thread thread2 = new Thread(threadGroup, "thread2");
        System.out.println(thread1.getThreadGroup());
        System.out.println(thread2.getThreadGroup());

    }
}
