package com.lzp.java.concurrent.threadcore.stopthread;

/**
 * 线程任务run方法内没有sleep和wait
 *
 * @author lzp
 * @date 2020/01/31
 */
public class RightWayStopThreadRunWithoutSleep implements Runnable {

    @Override
    public void run() {
        int num = 0;
        while (num < Integer.MAX_VALUE && !Thread.currentThread().isInterrupted()) {
            if (num % 10000 == 0) {
                System.out.println(num);
            }
            num++;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new RightWayStopThreadRunWithoutSleep());
        thread.start();
        Thread.sleep(1000);
        thread.interrupt();
    }
}
