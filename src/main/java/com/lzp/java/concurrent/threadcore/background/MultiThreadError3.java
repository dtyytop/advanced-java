package com.lzp.java.concurrent.threadcore.background;

import java.util.HashMap;
import java.util.Map;

/**
 * 演示逸出的一个案例
 *
 * @author lzp
 * @date 2020/02/18
 */
public class MultiThreadError3 {
    /**
     * 假设states仅用于保存读取的文件内容，而不希望被改变
     */
    private Map<String, String> states;

    public MultiThreadError3() {
        states = new HashMap<>();
        states.put("1", "星期一");
        states.put("2", "星期二");
        states.put("3", "星期三");
    }

    /**
     * 方法会导致逸出！！！
     *
     * @return
     */
    public Map<String, String> getStates() {
        return states;
    }

    /**
     * 方法会导致逸出！！！对副本操作，不影响内部的数据
     *
     * @return
     */
    public Map<String, String> getStatesImproved() {
        return new HashMap<>(states);
    }

    public static void main(String[] args) {
        MultiThreadError3 multiThreadError3 = new MultiThreadError3();
        Map<String, String> states = multiThreadError3.getStates();
        System.out.println(states.get("1"));
        states.remove("1");
        System.out.println(states.get("1"));
    }
}
