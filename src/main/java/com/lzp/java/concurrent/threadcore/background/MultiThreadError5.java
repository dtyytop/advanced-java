package com.lzp.java.concurrent.threadcore.background;

/**
 * 在构造器中使用事件监听器或者启动线程，造成逸出
 *
 * 线程延时10ms，MultiThreadError5正在循环，无法及时赋值
 *
 * @author lzp
 * @date 2020/02/18
 */
public class MultiThreadError5 {

    int count;

    public MultiThreadError5(MySource source) {
        // 注册监听器--放到最前面正好被子线程触发
        // count逸出-获取count值时机太早
        // 匿名内部类始终可以访问外部属性
        source.registerListener(new EventListener() {
            @Override
            public void onEvent(Event e) {
                System.out.println("\n我得到的数字是" + count);
            }
        });
        for (int i = 0; i < 10000; i++) {
            System.out.print(i);
        }
        count = 100;
    }

    public static void main(String[] args) {
        MySource mySource = new MySource();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // 触发事件
                mySource.eventCome(new Event() {
                });
            }
        }).start();
        MultiThreadError5 multiThreadsError5 = new MultiThreadError5(mySource);
    }

    /**
     * 观察者模式
     */
    static class MySource {

        private EventListener listener;

        void registerListener(EventListener eventListener) {
            this.listener = eventListener;
        }

        void eventCome(Event e) {
            if (listener != null) {
                listener.onEvent(e);
            } else {
                System.out.println("还未初始化完毕");
            }
        }

    }

    /**
     * 事件监听器
     */
    @FunctionalInterface
    interface EventListener {
        void onEvent(Event e);
    }

    /**
     * 事件接口
     */
    interface Event {

    }
}
