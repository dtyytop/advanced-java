package com.lzp.java.concurrent.threadcore.stopthread;

/**
 * 清除标志位方法--interrupted()
 *
 * @author lzp
 * @date 2020/02/02
 */
public class RightWayInterrupted {
    public static void main(String[] args) throws InterruptedException {
        Runnable task = () -> {
            for (; ; ) {
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        Thread.sleep(1000);
        thread.interrupt();
        System.out.println("线程实例.isInterrupted: " + thread.isInterrupted());
        System.out.println("线程实例.interrupted: " + thread.interrupted());
        System.out.println("Thread.interrupted: " + Thread.interrupted());
        System.out.println("线程实例.isInterrupted: " + thread.isInterrupted());
    }
}
