package com.lzp.java.concurrent.threadcore.createthreads.wrongways;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 使用Callable和FutureTask创建线程
 *
 * @author lzp
 * @date 2020/01/30
 */
public class CallableFutureTaskStyle {
    public static void main(String[] args) {
        // 创建任务，启动线程
        FutureTask<String> futureTask = new FutureTask<>(new CallableTask());
        new Thread(futureTask).start();
        try {
            String result = futureTask.get();
            System.out.println(result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}

class CallableTask implements Callable {
    @Override
    public String call() throws Exception {
        return "使用Callable和FutureTask创建线程";
    }
}
