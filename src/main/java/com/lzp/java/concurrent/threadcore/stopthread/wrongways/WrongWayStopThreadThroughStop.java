package com.lzp.java.concurrent.threadcore.stopthread.wrongways;

/**
 * 通过stop方法停止线程：错误的停止方法：
 * 用stop()来停止线程，会导致线程运行一半突然停止，
 * 没办法完成一个基本单位的操作，会造成脏数据。
 *
 * @author lzp
 * @date 2020/02/01
 */
public class WrongWayStopThreadThroughStop {
    public static void main(String[] args) throws InterruptedException {
        Runnable task = () -> {
            // 给五个班的孩子发糖
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 10; j++) {
                    System.out.println(i + "--" + j);
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        Thread.sleep(100);
        thread.stop();
        // thread.interrupt();
    }
}
