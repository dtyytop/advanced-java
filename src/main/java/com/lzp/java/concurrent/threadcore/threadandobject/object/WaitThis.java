package com.lzp.java.concurrent.threadcore.threadandobject.object;

/**
 * 测试this.wait()
 *
 * @author lzp
 * @date 2020/02/04
 */
public class WaitThis implements Runnable{
    @Override
    public void run() {
        synchronized (this) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Thread thread = new Thread(new WaitThis());
        thread.start();
    }
}
