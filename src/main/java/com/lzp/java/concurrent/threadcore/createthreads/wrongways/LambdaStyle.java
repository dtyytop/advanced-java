package com.lzp.java.concurrent.threadcore.createthreads.wrongways;

/**
 * 使用Lambda创建线程
 *
 * @author lzp
 * @date 2020/01/30
 */
public class LambdaStyle {
    public static void main(String[] args) {
        new Thread(() -> System.out.println(Thread.currentThread().getName())).start();
    }
}
