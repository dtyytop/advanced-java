package com.lzp.java.concurrent.threadcore.stopthread;

/**
 * run方法内包含sleep或者wait
 *
 * @author lzp
 * @date 2020/01/31
 */
public class RightWayStopThreadRunWithSleep {
    public static void main(String[] args) throws InterruptedException {
        Runnable task = () -> {
            int num = 0;
            while (!Thread.currentThread().isInterrupted() && num <= 300) {
                if (num % 100 == 0) {
                    System.out.println(num);
                }
                num++;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        Thread.sleep(500);
        thread.interrupt();
    }
}
