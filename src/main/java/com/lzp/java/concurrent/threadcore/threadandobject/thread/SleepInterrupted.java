package com.lzp.java.concurrent.threadcore.threadandobject.thread;

import java.util.concurrent.TimeUnit;

/**
 * 中断sleep中的线程
 *
 * @author lzp
 * @date 2020/02/09
 */
public class SleepInterrupted implements Runnable {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new SleepInterrupted());
        thread.start();
        Thread.sleep(100);
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(i);
            try {
                TimeUnit.MINUTES.sleep(1);
            } catch (InterruptedException e) {
                System.out.println("线程中断");
                e.printStackTrace();
            }
        }
    }
}
