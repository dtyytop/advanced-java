package com.lzp.java.concurrent.threadcore.threadandobject.thread;

/**
 * join代替写法
 *
 * @author lzp
 * @date 2020/02/09
 */
public class JoinPrinciple {
    public static void main(String[] args) throws InterruptedException {
        Thread mainThread = Thread.currentThread();
        Thread subThread = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "执行完毕");
        });
        subThread.start();
        System.out.println("等待子线程运行完毕");
        // subThread.join();
        // 等价代码
        synchronized (subThread) {
            subThread.wait();
        }
        System.out.println("join节点");
    }
}
