package com.lzp.java.concurrent.threadcore.uncaughtexception;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 自定义的未捕获异常处理器
 *
 * @author lzp
 * @date 2020/02/12
 */
public class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

    private String name;

    public MyUncaughtExceptionHandler(String name) {
        this.name = name;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Logger logger = Logger.getAnonymousLogger();
        logger.log(Level.WARNING, t.getName() + "线程终止", e);
        System.out.println(name + "捕获异常" + t.getName());
        // System.out.println("堆栈信息" + e);
    }
}
