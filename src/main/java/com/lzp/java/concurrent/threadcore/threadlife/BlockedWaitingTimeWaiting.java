package com.lzp.java.concurrent.threadcore.threadlife;

/**
 * 获取如题线程状态
 *
 * @author lzp
 * @date 2020/02/03
 */
public class BlockedWaitingTimeWaiting implements Runnable {
    public static void main(String[] args) throws InterruptedException {
        // 注意两个线程使用同一个任务
        Runnable task = new BlockedWaitingTimeWaiting();
        Thread thread1 = new Thread(task);
        Thread thread2 = new Thread(task);
        thread1.start();
        thread2.start();
        Thread.sleep(50);
        // TIME_WAITING
        System.out.println(thread1.getState());
        // BLOCKED
        System.out.println(thread2.getState());
        Thread.sleep(1000);
        // WAITING
        System.out.println(thread1.getState());
    }

    @Override
    public void run() {
        subTask();
    }

    private synchronized void subTask() {
        try {
            Thread.sleep(1000);
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
