package com.lzp.java.concurrent.threadcore.stopthread.wrongways;

/**
 * 看似可行，但是不够健壮的volitile方式
 *
 * @author lzp
 * @date 2020/02/01
 */
public class WrongWayVotile implements Runnable {

    private volatile boolean flag = false;

    @Override
    public void run() {
        // 打印100的倍数
        int num = 1;
        try {
            while (num <= 1000 && !flag) {
                if(num % 100 == 0) {
                    System.out.println(num);
                }
                num++;
                Thread.sleep(1);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Runnable task = new WrongWayVotile();
        Thread thread = new Thread(task);
        thread.start();
        Thread.sleep(1000);
        ((WrongWayVotile) task).flag = true;
    }
}
