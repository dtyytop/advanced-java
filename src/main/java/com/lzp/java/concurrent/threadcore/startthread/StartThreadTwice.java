package com.lzp.java.concurrent.threadcore.startthread;

/**
 * 启动一个线程两次
 *
 * @author lzp
 * @date 2020/01/31
 */
public class StartThreadTwice {
    public static void main(String[] args) {
        Thread thread = new Thread();
        thread.start();
        thread.start();
    }
}
