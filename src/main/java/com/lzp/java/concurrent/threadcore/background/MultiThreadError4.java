package com.lzp.java.concurrent.threadcore.background;

/**
 * 构造器未初始化完毕就对this赋值--得到一个不可靠的对象
 *
 * @author lzp
 * @date 2020/02/18
 */
public class MultiThreadError4 {
    static Point point;

    public static void main(String[] args) throws InterruptedException {
        new Thread(new PointMaker()).start();
        // 结果是1,0
        Thread.sleep(5);
        // 结果是1,1
        // Thread.sleep(105);
        System.out.println(point);
    }
}

class Point {
    private final int x;
    private final int y;

    public Point(int x, int y) {
        this.x = x;
        // 使用未初始化完成的Point引用初始化变量，并延时100ms
        MultiThreadError4.point = this;
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.y = y;
    }

    @Override
    public String toString() {
        return x + "," + y;
    }
}

class PointMaker implements Runnable {

    @Override
    public void run() {
        new Point(1, 1);
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
