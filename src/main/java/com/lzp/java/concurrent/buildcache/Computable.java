package com.lzp.java.concurrent.buildcache;

/**
 * 缓存设置接口
 *
 * @author lzp
 * @date 2021/3/14
 */
public interface Computable<K, V> {
    V compute(K key) throws Exception;
}
