package com.lzp.java.concurrent.buildcache;

import java.util.concurrent.TimeUnit;

/**
 * @author lzp
 * @Description: 缓存设置实现类, 不具备缓存能力
 * @date 2021/3/14
 */
public class ComputableImpl implements Computable<String, Integer> {

    @Override
    public Integer compute(String key) throws Exception {
        TimeUnit.SECONDS.sleep(3);
        return (int) (Math.random() * 1000);
    }
}
