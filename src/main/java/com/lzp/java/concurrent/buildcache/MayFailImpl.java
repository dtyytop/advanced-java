package com.lzp.java.concurrent.buildcache;

import java.util.concurrent.TimeUnit;

/**
 * @author lzp
 * @Description: 缓存设置实现类, 不具备缓存能力
 * @date 2021/3/14
 */
public class MayFailImpl implements Computable<String, Integer> {

    @Override
    public Integer compute(String key) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        if (Math.random() > 0.99) {
            throw new Exception();
        }
        return (int) (Math.random() * 1000);
    }
}
