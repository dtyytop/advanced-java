package com.lzp.java.concurrent.buildcache;

import java.util.HashMap;

/**
 * @author lzp
 * @Description: 使用HashMap实现缓存，不具备并发安全
 * @date 2021/3/14
 */
public class Cache1 {
    private final HashMap<String, Integer> cache = new HashMap<>();

    public Integer compute(String key) {
        Integer result = cache.get(key);
        if (result == null) {
            result = innerCompute(key);
            cache.put(key, result);
        }
        return result;
    }

    private Integer innerCompute(String key) {
        return (int) (Math.random() * 1000);
    }

    public static void main(String[] args) {
        Cache1 cache1 = new Cache1();
        System.out.println(cache1.compute("key"));
        System.out.println(cache1.compute("key"));
    }
}
