package com.lzp.java.concurrent.buildcache;

import java.util.Map;
import java.util.concurrent.*;

/**
 * @author lzp
 * @Description: 线程池的使用位置不同
 * @date 2021/3/14
 */
public class Cache7<K, V> implements Computable<K, V> {
    private final Map<K, Future<V>> cache = new ConcurrentHashMap<>();
    private final Computable<K, V> c;
    public final static ScheduledExecutorService pool = Executors.newScheduledThreadPool(10);

    public Cache7(Computable<K, V> c) {
        this.c = c;
    }

    @Override
    public V compute(K key) throws Exception {
        while (true) {
            Future<V> result = cache.get(key);
            if (result == null) {
                FutureTask<V> futureTask = new FutureTask<V>(() -> {
                    V val = c.compute(key);
                    pool.schedule(() -> {
                        expire(key); // 超时时间结束，清除缓存
                    }, 3000, TimeUnit.MILLISECONDS);
                    return val;
                });
                result = futureTask;
                result = cache.putIfAbsent(key, result);
                if (result == null) {
                    result = futureTask;
                    futureTask.run();
                }
            }
            try {
                return result.get();
            } catch (InterruptedException e) {
                cache.remove(key);
                throw e;
            } catch (ExecutionException e) {
                System.out.println("异常执行重试。。。");
                cache.remove(key); // 清除污染缓存！！
                // cache.replace() 新思路 生成一个新的任务，然后替换原来的任务。
            }
        }
    }

    private synchronized void expire(K key) {
        Future<V> future = cache.get(key);
        if (future != null) {
            if (future.isDone()) {
                System.out.println("任务已经被取消");
                future.cancel(true);
            }
            System.out.println("清除缓存。。。");
            cache.remove(key);
        }
    }

    public static void main(String[] args) throws Exception {
        Cache7<String, Integer> cache1 = new Cache7<>(new MayFailImpl());
        System.out.println(cache1.compute("key"));
        TimeUnit.SECONDS.sleep(5);
        System.out.println(cache1.compute("key"));
    }
}
