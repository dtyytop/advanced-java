package com.lzp.java.concurrent.immutable;

/**
 * @author lzp
 * @date 2020/05/05
 */
public class FinalDemo {
    public static void main(String[] args) {
        String a = "awecoder";
        final String b = "awe";
        String c = "awe";
        String d = b + "coder";
        String e = c + "coder";
        /**
         * JVM编译时发现b的值是不可变的，便会对d直接做编译优化，指向a所指向的字符串常量
         */
        System.out.println(a == d);
        System.out.println(a == e);

        /**
         * 通过方法获得，编译器无法确定final对象的值，不会进行编译器优化
         */
        final String f = getValue();
        String g = f + "coder";
        System.out.println(a == g);

    }

    private static String getValue() {
        return "awe";
    }
}
