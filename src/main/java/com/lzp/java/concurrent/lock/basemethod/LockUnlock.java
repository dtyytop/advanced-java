package com.lzp.java.concurrent.lock.basemethod;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * lock和unlock方法
 *
 * @author lzp
 * @date 2020/04/07
 */
public class LockUnlock {
    private static final Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "打印任务");
        } finally {
            lock.unlock();
        }
    }
}
