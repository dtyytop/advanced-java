package com.lzp.java.concurrent.lock.reentrantlock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 递归获取锁--检查可重入特性
 *
 * @author lzp
 * @date 2020/04/12
 */
public class RecursiveHoldLock {

    private static final ReentrantLock lock = new ReentrantLock();

    private static void accessLock() {
        lock.lock();
        try {
            if (lock.getHoldCount() < 5) {
                accessLock();
                System.out.println(Thread.currentThread().getName() + "第" + lock.getHoldCount() + "次获取到锁");
            }
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        new Thread(() -> accessLock()).start();
    }
}
