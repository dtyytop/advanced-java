package com.lzp.java.concurrent.deadlock;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 死锁策略2--检查与恢复策略 使用tryLock实现
 *
 * @author lzp
 * @date 2020/03/02
 */
public class TryLockDeadLock implements Runnable {
    private int flag;

    public TryLockDeadLock(int flag) {
        this.flag = flag;
    }

    private static final Lock lock1 = new ReentrantLock();
    private static final Lock lock2 = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new TryLockDeadLock(0));
        Thread thread2 = new Thread(new TryLockDeadLock(1));
        thread1.start();
        thread2.start();

    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            if (flag == 0) {
                try {
                    if (lock1.tryLock(1, TimeUnit.SECONDS)) {
                        System.out.println(Thread.currentThread().getName() + "获取lock1");
                        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(1000));
                        if (lock2.tryLock(1, TimeUnit.SECONDS)) {
                            System.out.println(Thread.currentThread().getName() + "获取lock2，此时拥有两把锁");
                            lock2.unlock();
                            lock1.unlock();
                            break;
                        } else {
                            System.out.println(Thread.currentThread().getName() + "获取lock2失败，重试");
                            lock1.unlock();
                            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(1000));
                        }
                    } else {
                        System.out.println(Thread.currentThread().getName() + "获取lock1失败，重试");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    if (lock2.tryLock(1, TimeUnit.SECONDS)) {
                        System.out.println(Thread.currentThread().getName() + "获取lock2");
                        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(1000));
                        if (lock1.tryLock(1, TimeUnit.SECONDS)) {
                            System.out.println(Thread.currentThread().getName() + "获取lock1，此时拥有两把锁");
                            lock1.unlock();
                            lock2.unlock();
                            break;
                        } else {
                            System.out.println(Thread.currentThread().getName() + "获取lock1失败，重试");
                            lock2.unlock();
                            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(1000));
                        }
                    } else {
                        System.out.println(Thread.currentThread().getName() + "获取lock2失败，重试");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
