package com.lzp.java.concurrent.deadlock;

/**
 * tt是锁
 * m2先执行，赋值2000；然后被线程t覆盖掉
 */
public class TestSync implements Runnable {
    int b = 100;

    synchronized void m1() throws InterruptedException {
        // Thread.sleep(5);
        b = 1000;
        Thread.sleep(500);
        System.out.println("b=" + b);
    }

    synchronized void m2() throws InterruptedException {
        Thread.sleep(250);
        b = 2000;
        System.out.println("b的临时值" + b);
    }

    public static void main(String[] args) throws InterruptedException {
        TestSync tt = new TestSync();
        Thread t = new Thread(tt);
        t.start();

        tt.m2();
        System.out.println("main thread b=" + tt.b);
    }

    @Override
    public void run() {
        try {
            m1();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}