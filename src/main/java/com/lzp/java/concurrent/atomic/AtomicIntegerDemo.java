package com.lzp.java.concurrent.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 原子类保证原子性，线程安全
 *
 * @author lzp
 * @date 2020/04/25
 */
public class AtomicIntegerDemo implements Runnable {
    private static final AtomicInteger atomicCount = new AtomicInteger();

    public void increaseAtomic() {
        atomicCount.incrementAndGet();
        // atomicInteger.getAndAdd(10);
    }

    private static volatile int count = 0;

    public void increase() {
        count++;
    }

    public static void main(String[] args) throws InterruptedException {
        AtomicIntegerDemo demo = new AtomicIntegerDemo();
        Thread thread1 = new Thread(demo);
        Thread thread2 = new Thread(demo);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println(atomicCount);
        System.out.println(count);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            increase();
            increaseAtomic();
        }
    }
}
