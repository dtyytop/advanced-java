package com.lzp.java.concurrent.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

/**
 * 高并发场景下，LongAdder性能优于AtomicLong
 *
 * @author lzp
 * @date 2020/04/25
 */
public class LongAdderConsumeTime {
    public static void main(String[] args) {
        LongAdder counter = new LongAdder();
        ExecutorService pool = new ThreadPoolExecutor(20, 20, 0, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            pool.execute(new Task(counter));
        }
        // 关闭线程池，自旋检测线程池状态
        pool.shutdown();
        while (!pool.isTerminated()) {
        }
        long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - start));
        System.out.println(counter.sum());

    }


    static class Task implements Runnable {
        private LongAdder counter;

        public Task(LongAdder counter) {
            this.counter = counter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                counter.increment();
            }
        }
    }
}
