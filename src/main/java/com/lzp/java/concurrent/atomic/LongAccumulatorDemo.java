package com.lzp.java.concurrent.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.stream.IntStream;

/**
 * LongAccumulator测试
 *
 * @author lzp
 * @date 2020/04/26
 */
public class LongAccumulatorDemo {
    public static void main(String[] args) {
        LongAccumulator accumulator = new LongAccumulator((x, y) -> x * y, 1);
        ExecutorService pool = Executors.newFixedThreadPool(10);
        IntStream.range(1,10).forEach(i -> pool.submit(() -> accumulator.accumulate(i)));
        pool.shutdown();
        while(!pool.isTerminated()){
        }
        System.out.println(accumulator.getThenReset());
    }
}
