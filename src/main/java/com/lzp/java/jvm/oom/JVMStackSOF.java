package com.lzp.java.jvm.oom;

/**
 * JVM栈溢出示例
 * -Xss128k
 *
 * @author lzp
 * @date 2020/6/16
 */
public class JVMStackSOF {
    private int statckLength = 1;

    public void stackLeak() {
        statckLength++;
        stackLeak();
    }

    public static void main(String[] args) {
        JVMStackSOF oom = new JVMStackSOF();
        try {
            oom.stackLeak();
        } catch (Exception e) {
            System.out.println("stack length: " + oom.statckLength);
            throw e;
        }
    }
}
