package com.lzp.java.jvm.oom;

/**
 * JVM创建线程导致内存溢出
 * -Xss10M
 *
 * @author lzp
 * @date 2020/6/16
 */
public class JVMStackOOM {
    private void dontStop() {
        while (true) {

        }
    }

    public void stackLeakByThread() {
        while (true) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    dontStop();
                }
            });
            thread.start();
        }
    }

    public static void main(String[] args) {
        JVMStackOOM oom = new JVMStackOOM();
        oom.stackLeakByThread();
    }
}
