package com.lzp.java.jvm.oom;

import java.util.ArrayList;
import java.util.List;

/**
 * 堆溢出案例
 * -Xms20m -Xmx20m -XX:+HeapDumpOnOutOfMemoryError
 *
 * @author lzp
 * @date 2020/6/16
 */
public class HeapOOM {
    static class OOMObject {

    }

    public static void main(String[] args) {
        List<OOMObject> list = new ArrayList<>();
        while (true) {
            list.add(new OOMObject());
        }
    }
}
