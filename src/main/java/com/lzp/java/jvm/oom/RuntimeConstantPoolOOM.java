package com.lzp.java.jvm.oom;

import java.util.HashSet;
import java.util.Set;

/**
 * 运行时常量池OOM
 * -Xmx6M
 *
 * @author lzp
 * @date 2020/6/17
 */
public class RuntimeConstantPoolOOM {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        short i = 0;
        while (true) {
            set.add(String.valueOf(i++).intern());
            System.out.println(i);
        }
    }
}
