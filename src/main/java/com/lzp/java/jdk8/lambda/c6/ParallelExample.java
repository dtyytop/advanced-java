package com.lzp.java.jdk8.lambda.c6;

import com.lzp.java.jdk8.lambda.pojo.Album;
import com.lzp.java.jdk8.lambda.pojo.SampleData;
import com.lzp.java.jdk8.lambda.pojo.Track;

import java.util.Arrays;

/**
 * @description: 并行化操作
 * @author: lzp
 * @createTime: 2021-07-19 07:45
 **/
public class ParallelExample {
    public static void main(String[] args) {


    }

    /**
     * 并行流操作 -- 计算专辑中曲目总长度
     *
     * @return
     */
    public int parallelArraySum() {
        return SampleData.albumList
                .parallelStream()
                .flatMap(Album::getTracks)
                .mapToInt(Track::getLength)
                .sum();
    }

    /**
     * 并行数组操作
     *
     * @param size
     * @return
     */
    public static double[] parallelInitial(int size) {
        double[] values = new double[size];
        Arrays.parallelSetAll(values, i -> i);
        return values;
    }
}
