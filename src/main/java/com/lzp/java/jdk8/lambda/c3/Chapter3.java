package com.lzp.java.jdk8.lambda.c3;

import com.lzp.java.jdk8.lambda.pojo.Album;
import com.lzp.java.jdk8.lambda.pojo.Artist;
import com.lzp.java.jdk8.lambda.pojo.SampleData;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @description: 第三章练习题
 * @author: lzp
 * @createTime: 2021-07-14 07:12
 **/
public class Chapter3 {

    /**
     * t1.a 求和
     */
    public static int addUp(Stream<Integer> numbers) {
        return numbers.reduce(0, (x, y) -> x + y);
    }

    /**
     * t1.b 打印姓名和国籍串
     */
    public static List<String> getNameAndOrigins(List<Artist> artists) {
        return artists.stream()
                .flatMap(artist -> Stream.of(artist.getName(), artist.getNationality()))
                .collect(Collectors.toList());
    }

    /**
     * t1.c
     */
    public static List<Album> getAlbumsWithAtMostThreeTracks(List<Album> input) {
        return input.stream()
//                .filter(album -> album.getTracks().count() < 3)
                .filter(album -> album.getTrackList().size() < 3)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        System.out.println(addUp(Stream.of(1, 2, 3)));
        System.out.println(getNameAndOrigins(SampleData.membersOfTheBeatles));
        System.out.println(getAlbumsWithAtMostThreeTracks(SampleData.albumList));
        System.out.println(t2(SampleData.getThreeArtists()));
    }

    /**
     * t2 统计艺术家总数
     */
    public static long t2(List<Artist> artists) {
        return artists.stream()
                .flatMap(artist -> artist.getMembers())
                .count();
    }

    /**
     * t6 统计字符串中小写字母个数
     */
    public static int countLetter(String str) {
        return (int) str.chars() // 获取流
                .filter(Character::isLowerCase)
                .count();
    }

    /**
     * t7 字符串列表中，找出包含最多小写字母的字符串
     */
    public static Optional<String> mostLowerCaseStr(List<String> strings) {
        return strings.stream()
                .max(Comparator.comparingInt(Chapter3::countLetter));
    }


}