package com.lzp.java.jdk8.lambda.c2;

import java.text.SimpleDateFormat;
import java.util.function.*;

/**
 * @description: 测试JDK中的通用函数式接口
 * @author: lzp
 * @createTime: 2021-07-11 20:16
 **/
public class LambdaAPI {
    /**
     * 使用ThreadLocal工厂方法，生成新的ThreadLocal对象
     */
    private static final ThreadLocal<SimpleDateFormat> dateFormatter
            = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

    public static void main(String[] args) {
        // 仅有一个参数，可以忽略括号
        Predicate<String> predicate = (String username) -> {
            return "admin".equals(username);
        };
        Predicate<String> predicate1 = username -> "admin".equals(username);
        System.out.println(predicate.test(""));
        //
        BiPredicate<String, String> biPredicate = (x, y) -> x.equals(y);
        //
        Consumer<String> consumer = x -> System.out.println(x);
        //
        BiConsumer<String, String> biConsumer = (x, y) -> System.out.println(x + y);
        //
        Function<String, Boolean> function = x -> x == null;
        //
        BiFunction<String, String, Boolean> biFunction = (x, y) -> x.equals(y);
        //
        Supplier<Boolean> supplier = () -> true;
        //
        UnaryOperator<Long> unaryOperator = x -> x + 1;
        //
        BinaryOperator<Long> binaryOperator = (x, y) -> x + y;

        // 无参数，代码块
        Runnable r = () -> {
            System.out.println("part1");
            System.out.println("part2");
        };

    }
}
