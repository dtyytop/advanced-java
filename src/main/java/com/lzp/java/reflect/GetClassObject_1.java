package com.lzp.java.reflect;

/**
 * 获取Class对象的三种方式
 *
 * @author lzp
 * @date 2020/6/9
 */
public class GetClassObject_1 {
    public static void main(String[] args) throws ClassNotFoundException {

        // 1 通过全类名加载，常用于加载配置文件、类
        // ClassNotFoundException 必然是类路径不正确
        Class clazz1 = Class.forName("com.lzp.java.reflect.User");

        // 2 通过类.class加载，常用于传参
        Class clazz2 = User.class;

        // 3 通过已有对象获取
        User user = new User();
        Class clazz3 = user.getClass();

        // 字节码只会被加载一次
        System.out.println(clazz1 == clazz2);
        System.out.println(clazz2 == clazz3);
    }
}
