package com.lzp.java.reflect;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * 根据配置文件获取类实例
 *
 * @author lzp
 * @date 2020/6/10
 */
public class GetClassObjectByConfig {
    public static void main(String[] args) throws Exception {
        // 加载配置文件
        InputStream is = GetClassObjectByConfig.class.getResourceAsStream("refenv.properties");
        Properties prop = new Properties();
        prop.load(is);

        // 获取属性
        String className = prop.getProperty("className");
        String methodName = prop.getProperty("methodName");

        // 运用反射
        Class<?> clazz = Class.forName(className);
        Object o = clazz.newInstance();
        Method method = clazz.getMethod(methodName);
        method.invoke(o);

    }
}
