package com.lzp.java.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 使用反射_2
 *
 * @author lzp
 * @date 2020/6/9
 */
public class UseRelectMethod_2 {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        // 获取User类的Class对象
        Class clazz = User.class;

        // 1 成员变量操作
        // 1.1 获取所有public成员变量
        System.out.println("-----------1.1------------");
        Field[] pubFields = clazz.getFields();
        for (Field field : pubFields) {
            System.out.println(field);
        }
        // 1.2 获取指定public变量
        System.out.println("-----------1.2------------");
        Field pubName = clazz.getField("name");
        System.out.println(pubName);
        // 1.3 获取所有变量
        System.out.println("-----------1.3------------");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }
        // 1.4 获取指定变量
        System.out.println("-----------1.4------------");
        Field id = clazz.getDeclaredField("id");
        User user = new User();
        // IllegalAccessException 没有权限对数据进行操作
        // 忽略访问权限修饰符的安全检查--暴力反射
        id.setAccessible(true);
        id.set(user, "11");
        System.out.println(id.get(user));

        // 2 构造方法--用于创建对象
        System.out.println("-----------2.1------------");
        Constructor constructor = clazz.getConstructor(String.class, String.class);
        System.out.println(constructor);
        // 创建对象 InvocationTargetException, InstantiationException
        Object lzp = constructor.newInstance("12", "lzp");
        System.out.println(lzp);
        System.out.println("-----------2.2------------");
        // 无参构造器，推荐使用Class对象的newInstance()方法
        Constructor constructor1 = clazz.getConstructor();
        System.out.println(constructor1.newInstance());
        System.out.println(clazz.newInstance());

        // 3 获取成员方法 --执行方法
        // 3.1 获取有参和无参方法
        System.out.println("-----------3.1------------");
        Method eat = clazz.getMethod("eat");
        eat.invoke(user);
        Method eat1 = clazz.getMethod("eat", String.class);
        eat1.invoke(user, "食物");
        // 3.2 获取所有public方法，包括Object方法
        System.out.println("-----------3.2------------");
        Method[] methods1 = clazz.getMethods();
        for (Method method : methods1) {
            System.out.println(method);
        }
        // 3.2 获取User中的所有方法
        System.out.println("-----------3.3------------");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method);
        }

        // 4 获取名称
        System.out.println(clazz.getName());
        System.out.println(clazz.getSimpleName());
    }
}
