package com.lzp.java.annotation.custom;

import java.lang.annotation.*;

/**
 * 元注解的使用
 *
 * @author lzp
 * @date 2020/6/7
 */
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.FIELD}) // 注解作用范围
@Retention(RetentionPolicy.RUNTIME) // 注解保存到运行时
@Documented // javadoc文档保留注解
@Inherited // 子类自动继承注解
public @interface MyAnnotation_3 {

    String[] value();
}
