package com.lzp.java.annotation.custom.demo;

/**
 * 小明定义的计算器类
 */
public class Calculator {

    @Check
    public void add() {
        System.out.println("1 + 0 =" + (1 + 0));
    }

    @Check
    public void sub() {
        System.out.println("1 - 0 =" + (1 - 0));
    }

    @Check
    public void mul() {
        System.out.println("1 * 0 =" + (1 * 0));
    }

    @Check
    public void div() {
        System.out.println("1 / 0 =" + (1 / 0));
    }

    @Check
    public void nullcheck() {
        String str = null;
        str.toString();
    }

    public void show() {
        System.out.println("永无bug...");
    }

}
