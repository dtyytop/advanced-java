package com.lzp.java.annotation.custom;

/**
 * @author lzp
 * @date 2020/6/7
 */
@ReturnType_2(value = 1, str = "", strs = {"11", "22"}, useenum = MyEnum.P1)
public class UseAnnotation {

    @MyAnnotation_1("11")
    public void method() {
        // value是特殊的属性
        // 数组只有一个值，可省略大括号
    }
}
