package com.lzp.java.annotation.custom;

/**
 * 注解的本质
 *
 * @author lzp
 * @date 2020/6/7
 */
public @interface MyAnnotation_1 {
    // public interface MyAnnotation extends java.lang.annotation.Annotation {
    // }
    String[] value();
}
