package com.lzp.java.annotation.custom;

/**
 * 返回值类型--不允许void返回类型
 *
 * @author lzp
 * @date 2020/6/7
 */
public @interface ReturnType_2 {

    // 基本数据类型
    int value();

    // 字符串
    String str();

    // 数组
    String[] strs();

    // 枚举
    MyEnum useenum();
}
