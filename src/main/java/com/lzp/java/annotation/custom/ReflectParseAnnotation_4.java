package com.lzp.java.annotation.custom;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 使用反射解析注解
 * 前提：不能改变该类的任何代码。可以创建任意类的对象，可以执行任意方法
 *
 * @author lzp
 * @date 2020/6/7
 */
@ConfigAnno(className = "com.lzp.java.annotation.custom.UseAnnotation", methodName = "method")
public class ReflectParseAnnotation_4 {
    public static void main(String[] args) throws Exception {
        // 1 获取当前类class
        Class<ReflectParseAnnotation_4> clazz = ReflectParseAnnotation_4.class;
        // 2 获取注解对象
        ConfigAnno configAnno = clazz.getAnnotation(ConfigAnno.class);
        // 3 调注解对象的方法，获取属性值
        // 其实就是在内存中生成了一个该注解接口的子类实现对象
        String className = configAnno.className();
        String methodName = configAnno.methodName();
        System.out.println(className);
        System.out.println(methodName);
        // 4 反射加载类
        // 4.1 加载类进入内存
        Class cls = Class.forName(className);
        // 4.2 创建实例
        Object obj = cls.newInstance();
        // 4.3 获取方法对象--需要方法是public
        Method method = cls.getMethod(methodName);
        // method.setAccessible(true);
        // 4.4 执行方法
        method.invoke(obj);

    }
}
