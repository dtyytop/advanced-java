package com.lzp.java.annotation;

/**
 * JDK中的预定义注解
 *
 * @Override：检测被该注解标注的方法是否是继承自父类(接口)的
 * @Deprecated：该注解标注的内容，表示已过时
 * @SuppressWarnings：压制警告 * 一般传递参数all  @SuppressWarnings("all")
 * 可以加在方法/类
 */
public class JdkPreDefinedAnnotation {
    @Override
    public String toString() {
        return super.toString();
    }

    @Deprecated
    public void outdateMethod() {
        // 已经过期的方法
    }

    @SuppressWarnings("all")
    public void method() {
        // 有警告的方法
    }
}
