package com.lzp.java.annotation;
/*
 * 注解，也叫元数据
 * 1.JDK1.5引入的特性，与类、接口、枚举一个层次
 * 2.说明程序
 * @注解
 *
 * 功能：
 * 1. 生成文档 例如@param @return参数描述
 * javadoc xxx.java --->index.html
 * 2. 编译检查--通过注解让编译器进行基本的检查 例如@Override
 * 3. 代码分析--使用反射
 *
 *
 * 注解三部分内容
 * 1.JDK预定义注解
 * @Override：检测被该注解标注的方法是否是继承自父类(接口)的
 * @Deprecated：该注解标注的内容，表示已过时
 * @SuppressWarnings：压制警告 * 一般传递参数all  @SuppressWarnings("all")
 * 可以加在方法/类
 *
 * 2.自定义注解
 * 注解本质上是一个接口，javac -- javap反编译
 * public interface MyAnnotation extends java.lang.annotation.Annotation {
 * }

 *
 * 3.使用注解
 * */