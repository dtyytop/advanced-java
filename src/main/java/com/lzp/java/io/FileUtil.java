package com.lzp.java.io;

import java.io.File;
import java.io.FileFilter;

public class FileUtil {
    /**
     * 递归打印目录中的Java文件
     *
     * @param dir
     */
    public static void printDir(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    if (file.getName().endsWith(".java")) {
                        System.out.println(file.getAbsolutePath());
                    }
                } else {
                    printDir(file);
                }
            }
        }
    }

    /**
     * 递归打印目录中的Java文件--使用文件过滤器
     *
     * @param dir
     */
    public static void printDirFilter(File dir) {
        File[] files = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".java") || pathname.isDirectory();
            }
        });
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    System.out.println(file.getAbsolutePath());
                } else {
                    printDirFilter(file);
                }
            }
        }
    }

    /**
     * 递归打印目录中的Java文件--使用lambda表达式
     *
     * @param dir
     */
    public static void printDirLambda(File dir) {
        File[] files = dir.listFiles(file ->
                file.getName().endsWith(".java") || file.isDirectory()
        );
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    System.out.println(file.getAbsolutePath());
                } else {
                    printDirLambda(file);
                }
            }
        }
    }

    /**
     * 删除目录以及目录下所有文件
     *
     * @param dir
     */
    public static void deleteDir(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteEmptyDir(file);
                } else {
                    file.delete();
                }
            }
        }
    }

    public static void deleteEmptyDir(File dir) {
        dir.delete();
    }
}
