package com.lzp.java.socket.tcp.demo;

import java.io.*;
import java.net.Socket;

/**
 * TCP客户端
 *
 * @author lzp
 * @date 2020/6/14
 */
public class FileClient {
    public static void main(String[] args) throws IOException {
        // 1. 创建本地字节输入流
        FileInputStream fis = new FileInputStream("1.jpg");

        // 建立客户端socket
        Socket socket = new Socket("127.0.0.1", 8889);
        // 网络输出流
        OutputStream os = socket.getOutputStream();
        // 读取本地文件
        int len = 0;
        byte[] b = new byte[1024];
        while ((len = fis.read(b)) != -1) {
            os.write(b, 0, len);
        }
        // !! 上传完文件，给服务器一个结束标记
        socket.shutdownOutput();

        // 获取网络字节输入流
        InputStream is = socket.getInputStream();
        while ((len = is.read(b)) != -1) {
            System.out.println(new String(b, 0, len));
        }

        fis.close();
        is.close();
        os.close();
        socket.close();

    }
}
