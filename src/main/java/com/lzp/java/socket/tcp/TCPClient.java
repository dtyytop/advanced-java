package com.lzp.java.socket.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * TCP客户端
 * 注意:
 * 1.客户端和服务器端进行交互,必须使用Socket中提供的网络流,不能使用自己创建的流对象
 * 2.当我们创建客户端对象Socket的时候,就会去请求服务器和服务器经过3次握手建立连接通路
 * 这时如果服务器没有启动,那么就会抛出异常ConnectException: Connection refused: connect
 *
 * @author lzp
 * @date 2020/6/14
 */
public class TCPClient {
    public static void main(String[] args) throws IOException {
        //1.创建一个客户端对象Socket,构造方法绑定服务器的IP地址和端口号
        Socket socket = new Socket("localhost", 8888);
        //2.使用Socket对象中的方法getOutputStream()获取网络字节输出流OutputStream对象
        OutputStream os = socket.getOutputStream();
        //3.写出数据
        os.write("客户端访问服务器".getBytes());

        //4.使用Socket对象中的方法getInputStream()获取网络字节输入流InputStream对象
        InputStream is = socket.getInputStream();

        //5.使用网络字节输入流InputStream对象中的方法read,读取服务器回写的数据
        byte[] b = new byte[1024];
        int len = is.read(b);
        System.out.println(new String(b, 0, len));

        //6.释放资源
        os.close();
        is.close();
        socket.close();
    }
}
