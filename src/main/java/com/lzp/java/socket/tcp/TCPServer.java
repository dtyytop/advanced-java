package com.lzp.java.socket.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * TCP服务端 -- 获取客户端发送来得socket对象，使用该对象的IO流读写数据
 *
 * @author lzp
 * @date 2020/6/14
 */
public class TCPServer {
    public static void main(String[] args) throws IOException {

        //1.创建服务器ServerSocket对象和系统要指定的端口号
        ServerSocket ss = new ServerSocket(8888);
        //2.使用ServerSocket对象中的方法accept,获取到请求的客户端对象Socket
        Socket socket = ss.accept();
        //3.使用Socket对象中的方法getInputStream()获取网络字节输入流InputStream对象
        InputStream is = socket.getInputStream();
        //4.使用网络字节输入流InputStream对象中的方法read,读取客户端发送的数据
        //4.1 创建字节数组
        byte[] b = new byte[1024];
        //4.2 数据读取到数组中
        int len = is.read(b);
        // 解析数组，并打印
        System.out.println(new String(b, 0, len));

        // 5. 获取客户端socket输出流
        OutputStream os = socket.getOutputStream();
        // 6. 输出数据
        os.write("服务端已收到消息".getBytes());

        // 7. 关闭资源
        os.close();
        is.close();
        socket.close();
    }
}
