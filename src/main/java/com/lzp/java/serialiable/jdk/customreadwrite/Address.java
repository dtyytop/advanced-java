package com.lzp.java.serialiable.jdk.customreadwrite;

public class Address {
    private String detail;

    public Address(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return detail;
    }
}