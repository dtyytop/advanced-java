package com.lzp.java.serialiable.jdk.fatherson;

import java.io.*;

/**
 * 父类不实现序列化，子类实现序列化，要求父类也实现序列化
 *
 * @author lzp
 * @date 2020/04/05
 */
public class Son extends Father implements Serializable {
    private String name;

    public Son(String name) {
        super(12);
        this.name = name;
    }

    public static void main(String[] args) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("oos.txt"));
            oos.writeObject(new Son("lzp"));
            oos.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            File file = new File("oos.txt");
            file.delete();
        }
    }
}
