package com.lzp.java.serialiable.jdk;

import java.io.*;

public class TransientTest {
    public static void main(String[] args) {
        try {
            User user = new User(1, "lzp", "account");
            System.out.println("序列化之前" + user);

            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/home/ac/workspace/tmp/user.txt"));
            oos.writeObject(user);
            oos.close();

            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("/home/ac/workspace/tmp/user.txt"));
            User readUser = (User) ois.readObject();
            System.out.println("序列化之后" + readUser);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
