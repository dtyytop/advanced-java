package com.lzp.java.serialiable.jdk;

import java.io.*;

/**
 * 测试静态变量不会被序列化
 */
public class TestStatic implements Serializable {

    private static final long serialVersionUID = 1L;
    public static int staticVar = 5;

    public static void main(String[] args) {
        try {
            // 初始时staticVar为5
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("result.obj"));
            out.writeObject(new TestStatic());
            out.close();
            // 序列化后修改为10
            TestStatic.staticVar = 10;
            ObjectInputStream oin = new ObjectInputStream(new FileInputStream("result.obj"));
            TestStatic t = (TestStatic) oin.readObject();
            oin.close();
            // 再读取，通过t.staticVar打印新的值
            System.out.println(t.staticVar);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            File file = new File("result.obj");
            file.delete();
        }
    }
}