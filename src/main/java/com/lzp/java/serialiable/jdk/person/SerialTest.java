package com.lzp.java.serialiable.jdk.person;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
 
public class SerialTest {
 
    public static void main(String[] args) {
        Persion p = new Persion(1L, "awecoder");
        System.out.println("person Seria:" + p);
        try {
            FileOutputStream fos = new FileOutputStream("Persion.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(p);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}