package com.lzp.java.serialiable.jdk.person;

import java.io.Serializable;

/**
 * A端序列化，B端反序列化，如果版本号相同(5种情况）
 */
public class Persion implements Serializable {

    private static final long serialVersionUID = 4359709211352400086L;
    public Long id;
    public String name;
    public final String userName;

    public Persion(Long id, String name) {
        this.id = id;
        this.name = name;
        userName = "lzp";
    }

    public String toString() {
        return id.toString() + "--" + name.toString();
    }
}