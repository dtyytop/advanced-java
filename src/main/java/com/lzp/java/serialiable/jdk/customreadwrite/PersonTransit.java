package com.lzp.java.serialiable.jdk.customreadwrite;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;

/**
 * 给出一个 PersonTransit 类，一个 Address 类，假设 Address 是其它 jar 包中的类，没实现序列化接口。
 * 请使用自定义的函数 writeObject 和 readObject 函数实现 PersonTransit 对象的序列化，要求反序列化后 address 的值正常。
 */
public class PersonTransit implements Serializable {

    private static final long serialVersionUID = -2268425365641325119L;

    private Long id;
    private String name;
    private Boolean male;
    private List<PersonTransit> friends;

    private transient Address address;

    public PersonTransit(Long id, String name, Boolean male, List<PersonTransit> friends) {
        this.id = id;
        this.name = name;
        this.male = male;
        this.friends = friends;
    }

    /**
     * writeObject、readObject、readResolve三个方法必须私有
     *
     * @param oos
     * @throws IOException
     */
    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        oos.writeObject(address.getDetail());
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        this.setAddress(new Address((String) ois.readObject()));
    }

    /***getter && setter***/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getMale() {
        return male;
    }

    public void setMale(Boolean male) {
        this.male = male;
    }

    public List<PersonTransit> getFriends() {
        return friends;
    }

    public void setFriends(List<PersonTransit> friends) {
        this.friends = friends;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "[" + id + "," + name + "," + friends + "," + address + "]";
    }
}

