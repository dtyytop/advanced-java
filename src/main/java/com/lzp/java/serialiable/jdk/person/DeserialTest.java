package com.lzp.java.serialiable.jdk.person;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
 
public class DeserialTest {
 
    public static void main(String[] args) {
        Persion p;
        try {
            FileInputStream fis = new FileInputStream("Persion.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            p = (Persion) ois.readObject();
            ois.close();
            System.out.println(p.toString());
            System.out.println(p.userName);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            File file = new File("Persion.txt");
            file.delete();
        }
    }
}