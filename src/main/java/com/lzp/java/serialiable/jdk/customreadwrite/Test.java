package com.lzp.java.serialiable.jdk.customreadwrite;

import java.io.*;
import java.util.ArrayList;

/**
 * @author lzp
 * @date 2020/04/05
 */
public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        PersonTransit personTransit = new PersonTransit(1L, "lzp", true, new ArrayList<>());
        personTransit.setAddress(new Address("beijing"));

        File file = new File("person.txt");
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("person.txt"))) {
            oos.writeObject(personTransit);
            oos.flush();
        }

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
        PersonTransit newInstance = (PersonTransit) ois.readObject();

        assert personTransit.getName().equals(newInstance.getName());
        assert personTransit.getAddress().getDetail().equals(newInstance.getAddress().getDetail());
        System.out.println(personTransit);
        System.out.println(newInstance);

        file.delete();

    }
}
