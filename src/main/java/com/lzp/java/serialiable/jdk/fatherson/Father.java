package com.lzp.java.serialiable.jdk.fatherson;

/**
 * @author lzp
 * @date 2020/04/05
 */
public class Father {
    private int age;

    // public Father() {
    // }

    public Father(int age) {
        this.age = age;
    }
}
