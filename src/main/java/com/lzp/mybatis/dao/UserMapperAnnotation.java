package com.lzp.mybatis.dao;

import com.lzp.mybatis.dataobject.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 注解mapper接口
 */
public interface UserMapperAnnotation {

    // 查询所有User
    @Select("select * from user")
    List<User> listAllUser();
}
