package com.lzp.mybatis.dao;

import com.lzp.mybatis.dataobject.Account;
import com.lzp.mybatis.dataobject.Person;

import java.util.List;

public interface PersonMapper {

    /**
     * 查询所有账户，同时还要获取到当前账户的所属用户信息
     */
    List<Person> listAllPersonAccount();

    /**
     * 查询所有用户以及他们的角色
     */
    List<Person> listAllPersonRole();
}
