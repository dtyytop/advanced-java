package com.lzp.mybatis.dao;

import com.lzp.mybatis.dataobject.User;
import com.lzp.mybatis.dataobject.UserWrapper;

import java.util.List;

public interface UserMapper {

    // 查询所有User
    List<User> listAllUser();

    // 新增User
    void saveUser(User user);

    // 更新User
    void updateUser(User user);

    // 删除User
    void deleteUser(Integer id);

    // 根据id查用户
    User findById(Integer id);

    // 根据姓名模糊搜索
    List<User> findByName(String username);

    // 使用实体类的包装对象查询
    List<User> findByUserWrapper(UserWrapper userWrapper);

    // 根据条件查询
    List<User> getUserBySelective(String username);

    List<User> getUserByIds(List<Integer> ids);
}
