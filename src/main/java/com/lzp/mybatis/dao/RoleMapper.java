package com.lzp.mybatis.dao;

import com.lzp.mybatis.dataobject.Role;

import java.util.List;

public interface RoleMapper {
    /**
     * 列出所有角色以及其包含的用户信息
     *
     * @return
     */
    List<Role> listAllRolePerson();
}
