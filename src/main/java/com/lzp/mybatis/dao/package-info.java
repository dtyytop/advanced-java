package com.lzp.mybatis.dao;

/*
分别包含三种mybatis开发方式:
1. 使用dao接口和xml方式开发
2. 使用dao接口注解方式开发
3. 使用dao接口及其实现类和xml方式开发
*/
