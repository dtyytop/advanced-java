package com.lzp.mybatis.custom.dao;

import com.lzp.mybatis.custom.dataobject.User;

import java.util.List;

public interface UserMapper {

    // 查询所有User
    List<User> listAllUser();
}
