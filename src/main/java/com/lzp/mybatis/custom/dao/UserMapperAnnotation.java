package com.lzp.mybatis.custom.dao;

import com.lzp.mybatis.custom.annotations.Select;
import com.lzp.mybatis.custom.dataobject.User;

import java.util.List;

/**
 * 注解mapper接口
 */
public interface UserMapperAnnotation {

    // 查询所有User
    @Select("select * from user")
    List<User> listAllUser();
}
