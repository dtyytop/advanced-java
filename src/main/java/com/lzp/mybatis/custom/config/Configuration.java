package com.lzp.mybatis.custom.config;

import java.util.HashMap;
import java.util.Map;

/**
 * mybatis-config.xml配置文件对应配置类
 * <p>
 * 包含数据库连接池属性、Mapper映射
 * </p>
 */
public class Configuration {
    private String driver; // 驱动
    private String url; // URL
    private String username; // 用户名
    private String password; // 密码

    private Map<String, Mapper> mappers = new HashMap<>(); // Mapper字典

    public Map<String, Mapper> getMappers() {
        return mappers;
    }

    public void setMappers(Map<String, Mapper> mappers) {
        this.mappers.putAll(mappers); // 追加，而不是直接赋值
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
