package com.lzp.mybatis.custom.config;

/**
 * Mapper对象类--CustomUserMapper.xml对应POJO类
 * <p>
 * （1）包含sql语句和结果类型
 * </p>
 */
public class Mapper {
    private String sql;
    private String resultType; // 实体类全限定类名

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }
}
