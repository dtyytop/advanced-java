package com.lzp.mybatis.custom.sqlsession;


import com.lzp.mybatis.custom.config.Configuration;
import com.lzp.mybatis.custom.sqlsession.defaults.DefaultSqlSessionFactory;
import com.lzp.mybatis.custom.utils.XmlConfigBuilder;

import java.io.InputStream;

/**
 * 用于创建SqlSessionFactory
 */
public class SqlSessionFactoryBuilder {

    /**
     * 根据配置输入流创建SqlSessionFactory
     *
     * @param inputStream
     * @return
     */
    public SqlSessionFactory build(InputStream inputStream) {
        Configuration configuration = XmlConfigBuilder.loadConfiguration(inputStream);
        return new DefaultSqlSessionFactory(configuration);
    }
}
