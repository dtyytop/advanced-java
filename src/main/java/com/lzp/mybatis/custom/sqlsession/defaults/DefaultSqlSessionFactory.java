package com.lzp.mybatis.custom.sqlsession.defaults;

import com.lzp.mybatis.custom.config.Configuration;
import com.lzp.mybatis.custom.sqlsession.SqlSession;
import com.lzp.mybatis.custom.sqlsession.SqlSessionFactory;

/**
 * SqlSessionFactory接口实现类
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * 通过Configuration属性创建SqlSession对象
     *
     * @return
     */
    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration);
    }
}
