package com.lzp.mybatis.custom.sqlsession;

/**
 * SqlSession接口
 * <p>
 * mybatis操作的主要接口，用于执行命令、获取Mapper、操作事务
 * </p>
 */
public interface SqlSession {

    /**
     * 根据Mapper接口字节码获取Mapper接口
     *
     * @param mapperClass 接口字节码
     * @param <T>
     * @return
     */
    <T> T getMapper(Class<T> mapperClass);

    /**
     * 释放SqlSession资源
     */
    void close();
}
