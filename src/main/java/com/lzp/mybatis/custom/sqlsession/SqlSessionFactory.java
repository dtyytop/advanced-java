package com.lzp.mybatis.custom.sqlsession;

/**
 * SqlSession工厂接口
 */
public interface SqlSessionFactory {

    /**
     * 创建一个新的SqlSession对象
     *
     * @return
     */
    SqlSession openSession();
}
