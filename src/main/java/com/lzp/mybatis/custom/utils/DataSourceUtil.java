package com.lzp.mybatis.custom.utils;

import com.lzp.mybatis.custom.config.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * 用于创建数据源的工具类
 */
public class DataSourceUtil {

    /**
     * 获取一个数据库连接
     *
     * @param config 配置类
     * @return
     */
    public static Connection getConnection(Configuration config) {
        try {
            // 注册驱动
            Class.forName(config.getDriver());
            // 获取连接
            return DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
