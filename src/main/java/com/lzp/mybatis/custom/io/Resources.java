package com.lzp.mybatis.custom.io;

import java.io.InputStream;

/**
 * 使用类加载器读取配置文件
 */
public class Resources {

    /**
     * 读取filePath配置，获取一个字节输入流
     *
     * @param filePath
     * @return
     */
    public static InputStream getResourceAsStream(String filePath) {
        // 通过当前类字节码获取类加载器，调用类加载器方法
        return Resources.class.getClassLoader().getResourceAsStream(filePath);
    }
}
