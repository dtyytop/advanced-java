package com.lzp.mybatis.dataobject;

/**
 * Account类用于演示一对一关联查询
 */
public class Account {
    private Integer id;
    private Integer pid;
    private Double money;
    private Person person;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", pid=" + pid +
                ", money=" + money +
                ", person=" + person +
                '}';
    }
}
