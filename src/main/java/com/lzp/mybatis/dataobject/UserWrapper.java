package com.lzp.mybatis.dataobject;

/**
 * User对象的一个包装类
 */
public class UserWrapper {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
