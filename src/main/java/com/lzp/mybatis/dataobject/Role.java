package com.lzp.mybatis.dataobject;

import java.io.Serializable;
import java.util.List;

public class Role implements Serializable {
    private int id;
    private String name;
    private List<Person> persons;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name=" + name +
                ", persons=" + persons +
                '}';
    }
}
