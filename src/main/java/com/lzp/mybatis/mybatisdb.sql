CREATE DATABASE mybatis;

USE mybatis;

-- 用户表
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL COMMENT '用户名称',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `sex` char(1) DEFAULT NULL COMMENT '性别',
  `address` varchar(256) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO mybatis.`user`
(id, username, birthday, sex, address)
VALUES(41, '用户1', '2018-02-28 07:47:08', '男', '北京');
INSERT INTO mybatis.`user`
(id, username, birthday, sex, address)
VALUES(42, '用户2', '2018-03-03 05:09:37', '女', '北京');

-----------
CREATE TABLE `person` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL COMMENT '用户名称',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO mybatis.`person`(id, name) VALUES(41, '用户1'),(42, '用户2'),(43, '用户3');


CREATE TABLE `account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `PID` int(11) default NULL COMMENT '用户编号',
  `MONEY` double default NULL COMMENT '金额',
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `account`(`ID`,`PID`,`MONEY`) values (1,41,1000),(2,42,1000),(3,41,2000);


CREATE TABLE `role` (
  `id` int(11) NOT NULL COMMENT '编号',
  `name` varchar(30) default NULL COMMENT '角色名称',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `role`(`id`,`name`) values (1,'架构师'),(2,'程序员'),(3,'产品经理');

CREATE TABLE `person_role` (
  `pid` int(11) NOT NULL COMMENT '用户编号',
  `rid` int(11) NOT NULL COMMENT '角色编号',
  PRIMARY KEY  (`pid`,`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `person_role`(`pid`,`rid`) values (41,1),(42,1),(41,2);