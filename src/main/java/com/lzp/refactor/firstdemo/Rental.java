package com.lzp.refactor.firstdemo;

/**
 * 租借类
 */
public class Rental {
    private Movie movie; // 影片
    private int daysRented; // 租借时间

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }


    /**
     * 计算租借费用
     */
    public double getCharge() {
        return movie.getCharge(daysRented);
    }

    /**
     * 积分计算仅与Rental有关--提取函数
     */
    public int getFrequentRenterPoints() {
        return movie.getFrequentRenterPoints(daysRented);
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }
}
