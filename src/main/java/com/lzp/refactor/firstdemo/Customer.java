package com.lzp.refactor.firstdemo;

import java.util.ArrayList;
import java.util.List;

/**
 * 顾客类
 */
public class Customer {
    private String name; // 姓名
    private List<Rental> rentals = new ArrayList<>(); // 租借影片信息

    public Customer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addRental(Rental rental) {
        rentals.add(rental);
    }

    public String statement() {
        String result = "租借记录：" + getName() + "\n";
        for (Rental rental : rentals) {
            result += "\t" + rental.getMovie().getTitle() + "\t" + String.valueOf(rental.getCharge()) + "\n";
        }
        result += "总花费：" + String.valueOf(getTotalCharge()) + "\n";
        result += "赚取积分数：" + String.valueOf(getFrequentRenterPoints());
        return result;
    }
    
    public String htmlStatement(){
        String result = "<h1>租借记录：<em>" + getName() + "</em></h1><p>\n";
        for (Rental rental : rentals) {
            result += rental.getMovie().getTitle() + ": " + String.valueOf(rental.getCharge()) + "<br>\n";
        }
        result += "<p>总花费：<em>" + String.valueOf(getTotalCharge()) + "</em><p>\n";
        result += "<p>赚取积分数：<em>" + String.valueOf(getFrequentRenterPoints()) + "</em><p>";
        return result;
    }

    /**
     * 计算总顾客积分--使用查询方法代替局部变量
     */
    private int getFrequentRenterPoints() {
        int result = 0;
        for (Rental rental : rentals) {
            result += rental.getFrequentRenterPoints();
        }
        return result;
    }

    /**
     * 计算全部租借费用--使用查询方法代替局部变量
     */
    private double getTotalCharge() {
        double result = 0;
        for (Rental rental : rentals) {
            result += rental.getCharge();
        }
        return result;
    }

}
