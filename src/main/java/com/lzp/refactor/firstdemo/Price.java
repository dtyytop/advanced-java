package com.lzp.refactor.firstdemo;

/**
 * 价格码抽象类--组合与集成
 */
public abstract class Price {
    public abstract int getPriceCode();

    /**
     * 计算租借费用
     * 1. 提取函数
     * 2. 移动函数--当前函数与Customer类无关
     * 3. switch最好只操作对象自己的数据
     * 4. 移动方法
     * 5. 使用子类重写
     */
    public abstract double getCharge(int daysRented);

    /**
     * 积分计算
     */
    public int getFrequentRenterPoints(int daysRented) {
        return 1;
    }
}
