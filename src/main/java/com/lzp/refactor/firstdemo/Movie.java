package com.lzp.refactor.firstdemo;

/**
 * 影片类
 */
public class Movie {
    public static final int REGULAR = 0; // 普通片
    public static final int NEW_RELEASE = 1; // 新片
    public static final int CHILDRENS = 2; // 儿童片

    private String title; // 影片名称
    private Price price; // 影片类型代码

    public Movie(String title, int priceCode) {
        this.title = title;
        setPriceCode(priceCode);
    }

    /**
     * 计算租借费用
     */
    public double getCharge(int daysRented) {
        return price.getCharge(daysRented);
    }

    /**
     * 计算积分
     */
    public int getFrequentRenterPoints(int daysRented) {
        return price.getFrequentRenterPoints(daysRented);
    }

    public String getTitle() {
        return title;
    }

    public int getPriceCode() {
        return price.getPriceCode();
    }

    public void setPriceCode(int priceCode) {
        switch (priceCode) {
            case REGULAR:
                price = new RegularPrice();
                break;
            case NEW_RELEASE:
                price = new NewReleasePrice();
                break;
            case CHILDRENS:
                price = new ChildrensPrice();
                break;
            default:
                throw new IllegalArgumentException("不正确的价格码");
        }
    }
}
