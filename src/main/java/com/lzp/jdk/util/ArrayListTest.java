package com.lzp.jdk.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArrayListTest {
    public static void main(String[] args) {
        Student student = new Student("lzp", 0);
        List<Student> students = new ArrayList<>();
        students.add(student);
        students.add(new Student("lzp", 1));
        students.add(new Student("lzp", 2));
        students.add(new Student("lzp", 3));
        students.add(new Student("lzp", 4));
        students.add(new Student("lzp", 5));
        System.out.println(students);
        Iterator iterator = students.iterator();
        int i = 0;
        while (iterator.hasNext()) {

            if (i % 3 != 2) {
                System.out.println(iterator.next());
            }
            if (i != 0 && i % 3 == 2) {
                iterator.remove();
            }
//            iterator.remove();
            i++;
        }
    }
}

class Student {
    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
