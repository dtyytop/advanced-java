package com.lzp.spring.core.autoscan;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 自动扫描配置类
 *
 * @author lzp
 * @date 2020/7/31
 */
@Configuration
@ComponentScan("com.lzp.spring.core.autoscan")  // 默认扫描扫描所在包
public class CDConfig {
}
