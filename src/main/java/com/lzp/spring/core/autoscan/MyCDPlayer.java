package com.lzp.spring.core.autoscan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author lzp
 * @date 2020/8/1
 */
@Component
public class MyCDPlayer implements CDPlayer {

    private CD cd;

    @Autowired
    public MyCDPlayer(CD cd) {
        this.cd = cd;
    }

    @Override
    public String play() {
        return cd.play();
    }

}
