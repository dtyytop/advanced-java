package com.lzp.spring.core.autoscan;

import org.springframework.stereotype.Component;

/**
 * CD POJO类
 *
 * @author lzp
 * @date 2020/7/31
 */
@Component
public class MyCD implements CD {

    private String title = "CD";

    private String author = "lzp";

    @Override
    public String play() {
        return (title + ": " + author);
    }

}
