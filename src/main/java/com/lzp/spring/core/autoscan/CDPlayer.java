package com.lzp.spring.core.autoscan;

/**
 * @author lzp
 * @date 2020/8/1
 */
public interface CDPlayer {

    String play();

}
