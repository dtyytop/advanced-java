package com.lzp.spring.core.config;

import com.lzp.spring.core.autoscan.CD;
import com.lzp.spring.core.autoscan.CDPlayer;
import com.lzp.spring.core.autoscan.MyCD;
import com.lzp.spring.core.autoscan.MyCDPlayer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 自动扫描配置类
 *
 * @author lzp
 * @date 2020/7/31 
 */
@Configuration
public class CDJavaConfig {

    @Bean
    public CD createMyCD() {
        return new MyCD();
    }

    @Bean
    public CDPlayer cdPlayer(CD cd) {
        return new MyCDPlayer(cd);
    }

    // @Bean
    // public CDPlayer cdPlayer() {
    //     return new CDPlayer();
    // }

}
