
CREATE TABLE `account_jdbc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '账户名',
  `balance` varchar(100) DEFAULT NULL COMMENT '账户余额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='JDBC事务测试表';

INSERT INTO mybatis.account_jdbc (name,balance) VALUES
('账户A','1000')
,('账户B','1000')
;