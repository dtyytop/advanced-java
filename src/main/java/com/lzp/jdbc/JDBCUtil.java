package com.lzp.jdbc;


import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.*;
import java.util.Properties;

/**
 * JDBC工具类，减少JDBC重复代码
 */
public class JDBCUtil {
    private static String driver;
    private static String url;
    private static String username;
    private static String password;

    /**
     * 读取数据库资源文件，加载JDBC驱动
     */
    static {
        try {
            Properties prop = new Properties();
            // 注意点：对于source目录下文件的获取方式，不是通过com/lzp/jdbc/jdbc.properties来获取的(FileNotFoundException)
            // 方式1：通过ClassLoader类加载器
            InputStream inputStream = JDBCUtil.class.getClassLoader().getResourceAsStream("com/lzp/jdbc/jdbc.properties");
            // 方法2：通过Class类
            // InputStream inputStream = JDBCUtil.class.getResourceAsStream("jdbc.properties");
            prop.load(inputStream);
            // 方式3：
            /*URL filepath = JDBCUtil.class.getClassLoader().getResource("com/lzp/jdbc/jdbc.properties");
            prop.load(new FileReader(filepath.getPath())); // getPath()方法可能获取到null*/

            driver = prop.getProperty("driver");
            url = prop.getProperty("url");
            username = prop.getProperty("username");
            password = prop.getProperty("password");
            Class.forName(driver);
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取连接
     *
     * @return
     */
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    /**
     * 关闭资源
     *
     * @param stmt
     * @param conn
     */
    public static void close(Statement stmt, Connection conn) {
        close(null, stmt, conn);
    }

    /**
     * 注意的是资源关闭操作不能放到同一个trycatch内，例如rs抛异常后，会导致stmt和conn也无法正常关闭
     *
     * @param rs
     * @param stmt
     * @param conn
     */
    public static void close(ResultSet rs, Statement stmt, Connection conn) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
