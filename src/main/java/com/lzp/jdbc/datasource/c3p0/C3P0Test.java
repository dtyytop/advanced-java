package com.lzp.jdbc.datasource.c3p0;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * C3P0连接池测试使用
 * --特别注意C3P0默认获取classpath根目录下的配置文件c3p0.properties/c3p0-config.xml
 */
public class C3P0Test {
    public static void main(String[] args) throws SQLException {
        DataSource dataSource = new ComboPooledDataSource();

        for (int i = 0; i < 10; i++) {
            // 从连接池获取连接
            Connection connection = dataSource.getConnection();
            System.out.println("----------------------------");
            System.out.println(i + "--" + connection);
            System.out.println("----------------------------");
            // 归还连接到连接池
            connection.close();
        }
    }
}
