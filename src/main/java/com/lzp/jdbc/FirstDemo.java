package com.lzp.jdbc;

import java.sql.*;

/**
 * JDBC入门测试类--JDBC执行过程
 */
public class FirstDemo {
    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        try {
            // 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            // 定义sql
            String sql = "update user set username = 'jdbc' where id = 63";
            // 获取连接
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mybatis", "root", "123456");
            // 创建Statement对象
            statement = connection.createStatement();
            // 执行sql
            int count = statement.executeUpdate(sql);
            // 结果处理
            System.out.println(count);
            assert count == 1;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
