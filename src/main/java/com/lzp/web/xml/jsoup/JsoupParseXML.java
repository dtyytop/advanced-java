package com.lzp.web.xml.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

/**
 * @author lzp
 * @date 2020/05/11
 */
public class JsoupParseXML {
    public static void main(String[] args) throws IOException {
        // 1. 根据路径获取Document对象
        String path = JsoupParseXML.class.getClassLoader().getResource("com/lzp/xml/jsoup/student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "UTF-8");
        // 2. 根据标签名称获取name列表
        Elements elements = document.getElementsByTag("student");

        // System.out.println(document);
        System.out.println(elements.size());
        // 获取第一个Element的文本数据
        System.out.println(elements.get(0).text());
        // 获取属性值
        System.out.println(elements.get(0).attr("number"));
    }
}
