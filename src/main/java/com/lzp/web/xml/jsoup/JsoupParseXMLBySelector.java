package com.lzp.web.xml.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

/**
 * 选择器查询，参考Selector类注释
 *
 * @author lzp
 * @date 2020/05/11
 */
public class JsoupParseXMLBySelector {
    public static void main(String[] args) throws IOException {
        String path = JsoupParseXMLBySelector.class.getClassLoader().getResource("com/lzp/xml/jsoup/student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");

        // 利用选择器查询
        // 查询name标签
        Elements elements = document.select("name");
        System.out.println(elements);
        System.out.println();

        // 查询id值为awe的元素
        Elements awes = document.select("#awe");
        System.out.println(awes);
        System.out.println();

        // 查询class值为coder的元素
        Elements coders = document.select(".coder");
        System.out.println(coders);
    }
}
