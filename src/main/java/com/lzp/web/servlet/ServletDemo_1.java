package com.lzp.web.servlet;

import javax.servlet.*;
import java.io.IOException;

/**
 * 实现Servlet接口
 * <p>
 * web.xml中配置Servlet和路由
 *
 * @author dtyy
 * @date 2020/8/8
 */
public class ServletDemo_1 implements Servlet {

    /**
     * 初始化方法
     * Servlet被创建时执行，只执行一次
     *
     * @param servletConfig
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("init.....");
    }

    /**
     * 获取Servlet的配置对象
     *
     * @return
     */
    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    /**
     * 每次Servlet被访问时，执行。执行多次
     *
     * @param servletRequest
     * @param servletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("hello servlet");
    }

    /**
     * 获取Servlet的信息，版本、作者等
     *
     * @return
     */
    @Override
    public String getServletInfo() {
        return null;
    }

    /**
     * 服务器正常关闭时执行
     */
    @Override
    public void destroy() {
        System.out.println("destroy.....");
    }

}
