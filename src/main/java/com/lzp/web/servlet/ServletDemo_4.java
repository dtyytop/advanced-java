package com.lzp.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 请求与响应方法
 *
 * @author dtyy
 * @date 2020/8/8
 */
@WebServlet("/demo4")
public class ServletDemo_4 extends HttpServlet {

    private static final long serialVersionUID = -3870904940335682738L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("hello http servlet -- GET");
        System.out.println(req);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("hello http servlet -- POST");
    }

}
