package com.lzp.web.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * HttpServlet 是专门针对HTTP的Servlet封装，实现了service方法
 * 内部对不同HTTP方法做了处理
 *
 * @author dtyy
 * @date 2020/8/8
 */
@WebServlet("/demo3")
public class ServletDemo_3 extends HttpServlet {

    private static final long serialVersionUID = -3870904940335682738L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("hello http servlet -- GET");
        System.out.println(req);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("hello http servlet -- POST");
    }

}
