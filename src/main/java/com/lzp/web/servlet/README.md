
## 1 Servlet介绍
### 1.1 Servlet

Servlet是一个接口，定义了Java类被Tomcat识别到的规范。

- 实现Servlet接口
- web.xml配置Servlet与路由（Servlet3.0注解配置不需要）

### 1.2 Servlet执行的原理
1. 当服务器接受到客户端浏览器的请求后，会解析请求URL路径，获取访问的Servlet的资源路径
2. 查找web.xml文件，是否有对应的<url-pattern>标签体内容。
3. 如果有，则在找到对应的<servlet-class>全类名(看到全类名，就要联想到反射)
4. tomcat会将字节码文件加载进内存，并且创建其对象
5. 调用其方法

### 1.3 Servlet生命周期和创建时机
生命周期
1. 被创建--执行init方法，只执行一次
2. 提供服务--service方法，可多次
3. 被销毁--执行destroy方法，执行一次
    1. 服务器正常关闭，才会执行destroy方法
    2. destroy方法是在servlet销毁之前执行的，用于释放资源。对象没了，怎么调方法呢。。

创建时机
1. 默认情况下，第一次被访问时，Servlet被创建
2. 通过load-on-startup配置为服务器启动时创建

特别注意！！！

Servlet是单例的，一个Servlet在内存中只存在一个对象。
多个用户同时访问时，可能会存在线程安全问题。因此，尽量不在Servlet中定义成员变量，即使定义了成员变量，也不要对变量进行修改（单纯的赋值操作是原子性的）。




### 1.4 Servlet 3.0 支持注解配置，不再依赖web.xml。
1. 实现Servlet接口
2. 使用@WebServlet注解，并且配置路由
```$xslt
http://localhost:8089/adwanced-java/demo2
```