package com.lzp.java8;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class StudentSortDemo {

    public static void main(String[] args) {
        List<Student> students = Arrays.asList(new Student[]{new Student("Ming", 120), new Student("Ping", 160)});
        // 匿名内部类
        students.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.getHeight().compareTo(o2.getHeight());
            }
        });
        // Lambda
        students.sort((o1, o2) -> o1.getHeight().compareTo(o2.getHeight()));
        // 使用Comparator静态辅助方法与Lambda
        students.sort(Comparator.comparing(o -> o.getHeight()));
        // 使用Comparator静态辅助方法与方法引用
        students.sort(Comparator.comparing(Student::getHeight));
    }

    public static class Student {
        private String name;
        private Integer height;

        public Student(String name, Integer height) {
            this.name = name;
            this.height = height;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getHeight() {
            return height;
        }

        public void setHeight(Integer height) {
            this.height = height;
        }
    }
}


