package com.lzp.java8;

import org.junit.Test;

import java.io.File;
import java.io.FileFilter;
import java.util.function.Function;
import java.util.function.Supplier;

public class MethodReferenceTest {
    /**
     * 测试类::实例方法
     */
    @Test
    public void testClassMethod() {
        File[] hiddenFilesInnerClass = new File(".").listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isHidden();
            }
        });
        File[] hiddenFilesLambda = new File(".").listFiles(file -> file.isHidden());
        File[] hiddenFilesMethodRef = new File(".").listFiles(File::isHidden);
    }

    /**
     * 测试类::静态方法
     */
    @Test
    public void testStaticMethod() {
        IntegerFunction function = Integer::parseInt;
        System.out.println(method(function, "12"));
    }

    public Integer method(IntegerFunction integerFunction, String str) {
        return integerFunction.apply(str);
    }

    public interface IntegerFunction {
        Integer apply(String str);
    }

    /**
     * 测试实例引用::实例方法
     */
    @Test
    public void testReferenceMethod(){
        MethodReferenceTest mrt = new MethodReferenceTest();
        System.out.println(method(mrt::parseInt,"42"));
    }


    public Integer parseInt(String str) {
        return new Integer(42);
    }

    /**
     * 测试构造方法
     * 类名::new
     */
    @Test
    public void testConstructMethod(){
        Supplier<Student> supplier = Student::new;
        Function<String,Student> function = Student::new;
        Student student = function.apply("lzp");
    }


    public class Student{
        private String name;

        public Student() {
            this.name = "lzp";
        }

        public Student(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
