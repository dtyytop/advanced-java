package com.lzp.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FilteringApples {
    public static void main(String[] args) {
        // 苹果存货
        List<Apple> inventory = Arrays.asList(new Apple(80, "green"), new Apple(155, "green"), new Apple(120, "red"));

        /* 原始解决方案 */
        // 遍历列表找到绿色苹果 [Apple{weight=80, color='green'}, Apple{weight=155, color='green'}]
        List<Apple> greenApples = filterGreenApples(inventory);
        // System.out.println(greenApples);

        // 遍历查找红色苹果 [Apple{weight=120, color='red'}]
        List<Apple> redApples = filterApplesByColor(inventory, "red");
        // System.out.println(redApples);

        // 遍历查找重量大于120的苹果
        List<Apple> weightBiggerThan120Apples = filterApplesByWeight(inventory, 120);
        // System.out.println(weightBiggerThan120Apples);

        // 以颜色和重量作为筛选条件
        List<Apple> heavyApples = filterApples(inventory, "", 150, false);
        // System.out.println(heavyApples);

        /* 使用谓词接口 -- 策略模式 */
        List<Apple> heavyApplesByAnonyInnerClass = filterApples2(inventory, new ApplePredicate() {
            @Override
            public boolean test(Apple apple) {
                return "green".equals(apple.color) && apple.getWeight() > 150;
            }
        });
        // System.out.println(heavyApplesByAnonyInnerClass);

        List<Apple> heavyApplesLambda = filterApples2(inventory, apple -> "green".equals(apple.color) && apple.getWeight() > 150);
        System.out.println(heavyApplesLambda);

    }

    public static List<Apple> filterGreenApples(List<Apple> inventory) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple : inventory) {
            if ("green".equals(apple.getColor())) {
                result.add(apple);
            }
        }
        return result;
    }

    public static List<Apple> filterApplesByColor(List<Apple> inventory, String color) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple : inventory) {
            if (apple.getColor().equals(color)) {
                result.add(apple);
            }
        }
        return result;
    }

    public static List<Apple> filterApplesByWeight(List<Apple> inventory, int weight) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple : inventory) {
            if (apple.getWeight() > weight) {
                result.add(apple);
            }
        }
        return result;
    }

    public static List<Apple> filterApples(List<Apple> inventory, String color, int weight, boolean flag) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple : inventory) {
            if ((flag && apple.getWeight() > weight) || (flag && apple.getColor().equals(color))) {
                result.add(apple);
            }
        }
        return result;
    }

    public static List<Apple> filterApples2(List<Apple> inventory, ApplePredicate predicate) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple : inventory) {
            if (predicate.test(apple)) {
                result.add(apple);
            }
        }
        return result;
    }

    /**
     * 谓词接口--返回boolean
     */
    public interface ApplePredicate {
        boolean test(Apple apple);
    }


    /**
     * 苹果实体类
     */
    public static class Apple {

        private int weight; // 重量
        private String color; // 颜色

        public Apple(int weight, String color) {
            this.weight = weight;
            this.color = color;
        }

        public Integer getWeight() {
            return weight;
        }

        public void setWeight(Integer weight) {
            this.weight = weight;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        @Override
        public String toString() {
            return "Apple{" + "weight=" + weight + ", color='" + color + '\'' + '}';
        }
    }

}


