package com.lzp.refactor;

import com.lzp.refactor.firstdemo.Customer;
import com.lzp.refactor.firstdemo.Movie;
import com.lzp.refactor.firstdemo.Rental;
import org.junit.Test;

public class FirstDemoTest {
    @Test
    public void testStatement() {
        Customer customer = new Customer("测试顾客");
        Movie children = new Movie("children", Movie.CHILDRENS);
        Movie newRelease = new Movie("new Release", Movie.NEW_RELEASE);
        Movie regular = new Movie("regular", Movie.REGULAR);
        customer.addRental(new Rental(regular, 3));
        customer.addRental(new Rental(newRelease, 1));
        customer.addRental(new Rental(children, 3));
        System.out.println(customer.statement());
    }

    @Test
    public void testHtmlStatement() {
        Customer customer = new Customer("测试顾客");
        Movie children = new Movie("children", Movie.CHILDRENS);
        Movie newRelease = new Movie("new Release", Movie.NEW_RELEASE);
        Movie regular = new Movie("regular", Movie.REGULAR);
        customer.addRental(new Rental(regular, 3));
        customer.addRental(new Rental(newRelease, 1));
        customer.addRental(new Rental(children, 3));
        System.out.println(customer.htmlStatement());
    }
}
