package com.lzp.mybatis.custom;

import com.lzp.mybatis.dao.PersonMapper;
import com.lzp.mybatis.dataobject.Person;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class PersonTest {

    private InputStream inputStream;
    private SqlSession session;
    private PersonMapper mapper;  // 接口

    @Before
    public void init() throws IOException {
        // 读取配置文件
        String resource = "com/lzp/mybatis/mybatis-config.xml";
        inputStream = Resources.getResourceAsStream(resource);
        // 创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession实例
        session = sqlSessionFactory.openSession();
        // 创建dao接口代理对象
        mapper = session.getMapper(PersonMapper.class);
    }

    @After
    public void destroy() throws IOException {
        session.commit();
        session.close();
        inputStream.close();
    }


    @Test
    public void listAllPersonAccount() throws IOException {
        // 5 业务查询
        List<Person> personList = mapper.listAllPersonAccount();
        System.out.println("-----------------------");
        for (Person person : personList) {
            System.out.println(person);
        }
        System.out.println("-----------------------");
    }

    @Test
    public void listAllPersonRole() throws IOException {
        // 5 业务查询
        List<Person> personList = mapper.listAllPersonRole();
        System.out.println("-----------------------");
        for (Person person : personList) {
            System.out.println(person);
        }
        System.out.println("-----------------------");
    }
}
