package com.lzp.mybatis.custom;

import com.lzp.mybatis.custom.dao.UserMapper;
import com.lzp.mybatis.custom.dao.UserMapperAnnotation;
import com.lzp.mybatis.custom.dataobject.User;
import com.lzp.mybatis.custom.io.Resources;
import com.lzp.mybatis.custom.sqlsession.SqlSession;
import com.lzp.mybatis.custom.sqlsession.SqlSessionFactory;
import com.lzp.mybatis.custom.sqlsession.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 针对于自定义mybatis实现过程的一些测试
 */
public class CustomUserTest {

    @Test
    public void customXmlUserTest() throws IOException {
        // 1 读取配置文件，通过类加载器获得InputStream
        String resource = "com/lzp/mybatis/custom/dao/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 2 创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3 获取SqlSession实例
        SqlSession session = sqlSessionFactory.openSession();
        // 4 创建dao接口代理对象
        UserMapper mapper = session.getMapper(UserMapper.class);
        // 5 业务查询
        List<User> userList = mapper.listAllUser();
        System.out.println("-----------------------");
        for(User user : userList) {
            System.out.println(user);
        }
        System.out.println("-----------------------");
        // 6 释放资源
        session.close();
        inputStream.close();
    }

    @Test
    public void listAllUserTestAnnotation() throws IOException {
        // 1 读取配置文件
        String resource = "com/lzp/mybatis/custom/dao/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 2 创建SqlSessionFactory -- 构建者模式
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3 获取SqlSession实例 -- 工厂模式
        SqlSession session = sqlSessionFactory.openSession();
        // 4 创建dao接口代理对象，而不是直接操作SqlSession(详见官网) -- 代理模式
        UserMapperAnnotation mapper = session.getMapper(UserMapperAnnotation.class);
        // 5 业务查询
        List<User> userList = mapper.listAllUser();
        System.out.println("-----------------------");
        for(User user : userList) {
            System.out.println(user);
        }
        System.out.println("-----------------------");
        // 6 释放资源
        session.close();
        inputStream.close();
    }
}
