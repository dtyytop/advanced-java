package com.lzp.mybatis;

import com.lzp.mybatis.dao.UserMapperImpl;
import com.lzp.mybatis.dataobject.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * 测试增删改查
 */
public class UserImplTest {
    private InputStream inputStream;
    private UserMapperImpl mapperImpl; // 实现类

    @Before
    public void init() throws IOException {
        // 读取配置文件
        String resource = "com/lzp/mybatis/mybatis-config.xml";
        inputStream = Resources.getResourceAsStream(resource);
        // 创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        /* 仅用于实现类方式*/
        mapperImpl = new UserMapperImpl(sqlSessionFactory);
    }

    @After
    public void destroy() throws IOException {
        inputStream.close();
    }


    /**
     * 测试查询
     *
     * @throws IOException
     */
    @Test
    public void testListAllUser() throws IOException {
        // 5 业务查询
        List<User> userList = mapperImpl.listAllUser();
        System.out.println("-----------------------");
        for (User user : userList) {
            System.out.println(user);
        }
        System.out.println("-----------------------");
    }

    /**
     * 测试新增用户
     */
    @Test
    public void testSaveUserImpl() {
        User user = new User();
        user.setAddress("浙江杭州");
        user.setUsername("雨夜");
        user.setBirthday(new Date());
        user.setSex("M");

        System.out.println("-----------------------");
        System.out.println("保存前：" + user);
        System.out.println("-----------------------");
        mapperImpl.saveUser(user);
        System.out.println("-----------------------");
        System.out.println("保存后：" + user);
        System.out.println("-----------------------");
    }

}
