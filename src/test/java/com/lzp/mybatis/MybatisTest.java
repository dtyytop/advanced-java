package com.lzp.mybatis;

import com.lzp.mybatis.dao.UserMapper;
import com.lzp.mybatis.dao.UserMapperAnnotation;
import com.lzp.mybatis.dao.UserMapperImpl;
import com.lzp.mybatis.dataobject.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MybatisTest {

    /**
     * 入门测试案例--xml方式开发
     *
     * @throws IOException
     */
    @Test
    public void listAllUserTest() throws IOException {
        // 1 读取配置文件
        // web服务器访问文件路径主要有两种：
        //（1）ServletContext的getRealPath()方法；
        //（2）Resources.getResource() 使用类加载器读取类路径的配置文件
        String resource = "com/lzp/mybatis/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 2 创建SqlSessionFactory -- 构建者模式
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3 获取SqlSession实例 -- 工厂模式
        SqlSession session = sqlSessionFactory.openSession();
        // 4 创建dao接口代理对象，而不是直接操作SqlSession(详见官网) -- 代理模式
        UserMapper mapper = session.getMapper(UserMapper.class);
        // 5 业务查询
        List<User> userList = mapper.listAllUser();
        System.out.println("-----------------------");
        for (User user : userList) {
            System.out.println(user);
        }
        System.out.println("-----------------------");
        // 6 释放资源
        session.close();
        inputStream.close();
    }

    /**
     * 入门测试案例--使用注解方式开发
     *
     * @throws IOException
     */
    @Test
    public void listAllUserTestAnnotation() throws IOException {
        // 1 读取配置文件
        String resource = "com/lzp/mybatis/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 2 创建SqlSessionFactory -- 构建者模式
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3 获取SqlSession实例 -- 工厂模式
        SqlSession session = sqlSessionFactory.openSession();
        // 4 创建dao接口代理对象，而不是直接操作SqlSession(详见官网) -- 代理模式
        UserMapperAnnotation mapper = session.getMapper(UserMapperAnnotation.class);
        // 5 业务查询
        List<User> userList = mapper.listAllUser();
        System.out.println("-----------------------");
        for (User user : userList) {
            System.out.println(user);
        }
        System.out.println("-----------------------");
        // 6 释放资源
        session.close();
        inputStream.close();
    }


    /**
     * 入门测试案例--使用dao接口实现类配合xml进行开发
     *
     * @throws IOException
     */
    @Test
    public void listAllUserTestImpl() throws IOException {
        // 1 读取配置文件
        String resource = "com/lzp/mybatis/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 2 创建SqlSessionFactory -- 构建者模式
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3 获取SqlSession实例 -- 工厂模式
        UserMapperImpl mapper = new UserMapperImpl(sqlSessionFactory);
        // 5 业务查询
        List<User> userList = mapper.listAllUser();
        System.out.println("-----------------------");
        for (User user : userList) {
            System.out.println(user);
        }
        System.out.println("-----------------------");
        // 6 释放资源
        inputStream.close();
    }


}
