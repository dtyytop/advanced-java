package com.lzp.mybatis;

import com.lzp.mybatis.dao.AccountMapper;
import com.lzp.mybatis.dataobject.Account;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class AccountTest {

    private InputStream inputStream;
    private SqlSession session;
    private AccountMapper mapper;  // 接口

    @Before
    public void init() throws IOException {
        // 读取配置文件
        String resource = "com/lzp/mybatis/mybatis-config.xml";
        inputStream = Resources.getResourceAsStream(resource);
        // 创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession实例
        session = sqlSessionFactory.openSession();
        // 创建dao接口代理对象
        mapper = session.getMapper(AccountMapper.class);
    }

    @After
    public void destroy() throws IOException {
        session.commit();
        session.close();
        inputStream.close();
    }


    /**
     * 测试查询
     *
     * @throws IOException
     */
    @Test
    public void testListAllAccount() throws IOException {
        // 5 业务查询
        List<Account> accountList = mapper.listAllAcount();
        System.out.println("-----------------------");
        for (Account account : accountList) {
            System.out.println(account);
        }
        System.out.println("-----------------------");
    }
}
