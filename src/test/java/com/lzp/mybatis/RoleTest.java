package com.lzp.mybatis;

import com.lzp.mybatis.dao.RoleMapper;
import com.lzp.mybatis.dataobject.Role;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class RoleTest {

    private InputStream inputStream;
    private SqlSession session;
    private RoleMapper mapper;  // 接口

    @Before
    public void init() throws IOException {
        // 读取配置文件
        String resource = "com/lzp/mybatis/mybatis-config.xml";
        inputStream = Resources.getResourceAsStream(resource);
        // 创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession实例
        session = sqlSessionFactory.openSession();
        // 创建dao接口代理对象
        mapper = session.getMapper(RoleMapper.class);
    }

    @After
    public void destroy() throws IOException {
        session.commit();
        session.close();
        inputStream.close();
    }


    /**
     * 测试查询
     *
     * @throws IOException
     */
    @Test
    public void testListAllRole() throws IOException {
        // 5 业务查询
        List<Role> roleList = mapper.listAllRolePerson();
        System.out.println("-----------------------");
        for (Role role : roleList) {
            System.out.println(role);
        }
        System.out.println("-----------------------");
    }
}
