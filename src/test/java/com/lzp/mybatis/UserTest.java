package com.lzp.mybatis;

import com.lzp.mybatis.dao.UserMapper;
import com.lzp.mybatis.dataobject.User;
import com.lzp.mybatis.dataobject.UserWrapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.sound.midi.MidiSystem;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 测试增删改查
 */
public class UserTest {
    private InputStream inputStream;
    private SqlSession session;
    private UserMapper mapper;  // 接口

    @Before
    public void init() throws IOException {
        // 读取配置文件
        String resource = "com/lzp/mybatis/mybatis-config.xml";
        inputStream = Resources.getResourceAsStream(resource);
        // 创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession实例
        session = sqlSessionFactory.openSession();
        // 创建dao接口代理对象
        mapper = session.getMapper(UserMapper.class);
    }

    @After
    public void destroy() throws IOException {
        // 提交事务（Test会将自动提交设置为false，操作结束后会进行回滚）
        // 即使事务回滚，依然会占用主键id
        session.commit();
        session.close();
        inputStream.close();
    }


    /**
     * 测试查询
     *
     * @throws IOException
     */
    @Test
    public void testListAllUser() throws IOException {
        // 5 业务查询
        List<User> userList = mapper.listAllUser();
        System.out.println("-----------------------");
        for (User user : userList) {
            System.out.println(user);
        }
        System.out.println("-----------------------");
    }

    /**
     * 测试新增用户
     */
    @Test
    public void testSaveUser() {
        User user = new User();
        user.setAddress("浙江杭州");
        user.setUsername("雨夜");
        user.setBirthday(new Date());
        user.setSex("M");

        System.out.println("-----------------------");
        System.out.println("保存前：" + user);
        System.out.println("-----------------------");
        mapper.saveUser(user);
        System.out.println("-----------------------");
        System.out.println("保存后：" + user);
        System.out.println("-----------------------");
    }

    /**
     * 测试新增用户
     */
    @Test
    public void testUpdateUser() {
        User user = new User();
        user.setId(44);
        user.setAddress("浙江杭州");
        user.setUsername("雨夜2");
        user.setBirthday(new Date());
        user.setSex("F");

        mapper.updateUser(user);
    }

    /**
     * 删除用户
     */
    @Test
    public void testDeleteUser() {
        mapper.deleteUser(44);
    }

    /**
     * 根据id查用户
     */
    @Test
    public void testFindById() {
        User user = mapper.findById(41);
        System.out.println("-----------------------");
        System.out.println(user);
        System.out.println("-----------------------");
    }

    /**
     * 根据姓名模糊搜索
     * <p>
     * 测试占位符与拼接符的使用，以及模糊搜索
     * </p>
     */
    @Test
    public void testFindByName() {
        List<User> userList = mapper.findByName("%用户%");// 占位符
        // List<User> userList = mapper.findByName("用户");// 拼接符
        System.out.println("-----------------------");
        for (User user : userList) {
            System.out.println(user);
        }
        System.out.println("-----------------------");
    }

    /**
     * 使用实体类的包装对象查询
     * <p>
     * 测试OGNL语言的使用
     * </p>
     */
    @Test
    public void testFindByWrapper() {
        UserWrapper userWrapper = new UserWrapper();
        User prop = new User();
        prop.setUsername("雨夜");
        userWrapper.setUser(prop);
        List<User> userList = mapper.findByUserWrapper(userWrapper);
        System.out.println("-----------------------");
        for (User user : userList) {
            System.out.println(user);
        }
        System.out.println("-----------------------");
    }

    /**
     * where和if标签使用
     */
    @Test
    public void testGetUserBySelective() {
        List<User> userList = mapper.getUserBySelective("雨夜");
        for (User user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void testGetUserByIds(){
        List<Integer> ids = new ArrayList<>();
        ids.add(51);
        ids.add(52);
        List<User> userList = mapper.getUserByIds(ids);
        for (User user : userList) {
            System.out.println(user);
        }
    }
}
