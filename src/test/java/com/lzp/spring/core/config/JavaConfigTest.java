package com.lzp.spring.core.config;

import com.lzp.spring.core.autoscan.AutoScanTest;
import com.lzp.spring.core.autoscan.CD;
import com.lzp.spring.core.autoscan.CDPlayer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

/**
 * @author lzp
 * @date 2020/8/1
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CDJavaConfig.class)
public class JavaConfigTest {

    private static final Logger logger =
            LoggerFactory.getLogger(JavaConfigTest.class);

    @Autowired
    private CD cd;

    @Autowired
    private CDPlayer cdPlayer;

    @Test
    public void testCDCreate() {
        assertNotNull(cd);
    }

    @Test
    public void testCDPlayer() {
        assertNotNull(cdPlayer);
        logger.info(cdPlayer.play());
    }
}
