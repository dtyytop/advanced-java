package com.lzp.spring.core.autoscan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

/**
 * @author lzp
 * @date 2020/7/31
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CDConfig.class)
public class AutoScanTest {

    private static final Logger logger =
            LoggerFactory.getLogger(AutoScanTest.class);

    @Autowired
    private CD cd;

    @Autowired
    private CDPlayer cdPlayer;

    @Test
    public void testCDCreate() {
        assertNotNull(cd);
    }

    @Test
    public void testCDPlayer() {
        assertNotNull(cdPlayer);
        logger.info(cdPlayer.play());
    }

}
