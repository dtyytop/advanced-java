package com.lzp.java.io;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 字节流测试
 */
public class ByteStreamTest {
    @Before
    public void init() throws IOException {
        File root = new File("tmp");
        root.mkdir();

        // 初始化一个文件，作为拷贝原始文件
        File file = new File("tmp/src.txt");
        FileOutputStream fos = new FileOutputStream(file);
        fos.write("abcdedf\n".getBytes());
        fos.write("abcdedfg".getBytes());
        fos.close();
    }

    @After
    public void destroy() {
        File tmpDir = new File("tmp");
        FileUtil.deleteDir(tmpDir);
        tmpDir.delete();
    }

    /**
     * 测试写出字节、字节数组到文件
     *
     * @throws IOException
     */
    @Test
    public void testFosWriteFile() throws IOException {
        // 第二个参数append表示是否追加到文件末尾
        FileOutputStream fos = new FileOutputStream("tmp/fos.txt");
        // 写出一个字节 ASCII 100对应字符d
        fos.write(100);
        // 写出换行
        fos.write("\n".getBytes());
        // 写出字节数组
        byte[] bytes = "雨夜".getBytes();
        fos.write(bytes);
        fos.close();
    }

    /**
     * 测试文件读入流
     */
    @Test
    public void testFisReadFile() throws IOException {
        File file = new File("tmp/fis.txt");
        FileOutputStream fos = new FileOutputStream(file);
        fos.write("abcde".getBytes());
        fos.close();

        // 一次读取一个字节
        FileInputStream fis = new FileInputStream(file);
        int b;
        while ((b = fis.read()) != -1) {
            System.out.println((char) b);
        }

        // 读取字节数组
        FileInputStream fis2 = new FileInputStream(file);
        byte[] bs = new byte[2];
        int len; // 当前读取有效字节个数
        while ((len = fis2.read(bs)) != -1) {
            // 每次读取打印，读取到字节数组中的有效字节（最后一个读取，数组通常未充满）
            System.out.println(new String(bs, 0, len));
        }
    }

    /**
     * 测试字节流拷贝
     *
     * @throws IOException
     */
    @Test
    public void testByteStreamCopy() throws IOException {
        FileInputStream fis = new FileInputStream(new File("tmp/src.txt"));
        FileOutputStream fos = new FileOutputStream(new File("tmp/src_copy.txt"));

        int len; // 有效字节个数
        byte[] bs = new byte[3];
        while ((len = fis.read(bs)) != -1) {
            fos.write(bs, 0, len);
            fos.flush();
        }
        fis.close();
        fos.close();
    }
}
