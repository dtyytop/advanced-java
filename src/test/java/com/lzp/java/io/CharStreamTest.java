package com.lzp.java.io;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

/**
 * 测试字符流--字符流，只能操作文本文件，不能操作图片，视频等非文本文件。
 */
public class CharStreamTest {

    @Before
    public void init() throws IOException {
        FileWriter fw = new FileWriter("tmp/fr.txt");
        fw.write("abcdefg\n");
        fw.write("abcdefg\n");
        fw.close();
    }

    @After
    public void destroy() {
        File tmpDir = new File("tmp");
        FileUtil.deleteDir(tmpDir);
        tmpDir.delete();
    }

    /**
     * 写出字符到文件
     * idea对应UTF-8编码，eclipse对应gbk编码
     *
     * @throws IOException
     */
    @Test
    public void testWriteChars2File() {
        FileWriter fw = null;
        try {
            fw = new FileWriter("tmp/fw.txt");

            // 写出单个字符到文件
            fw.write(100);
            fw.write('d');
            fw.write(30000); // 对应一个汉字

            // 写出字符数组到文件
            fw.write("\n大唐雨夜\n".toCharArray());
            fw.write("大唐雨夜".toCharArray(), 0, 2);

            // 写出字符串
            fw.write("\n大唐雨夜\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 特别注意：区别于FileInputStream
            // 如果不关闭，数据只是保存到缓冲区，并未保存到文件。然而流关闭后，无法继续写出数据
            // 既要写出数据，又不关闭流，采用flush方法
            try {
                if (fw != null) {
                    fw.flush(); // 刷新缓存区
                    fw.close(); // 刷新缓存区，并关闭流
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 测试字符输入
     */
    @Test
    public void testReadCharsFromFile() throws IOException {
        /*单个字符读取*/
        FileReader fr = new FileReader("tmp/fr.txt");
        // 定义变量，保存字符
        int b;
        while ((b = fr.read()) != -1) {
            System.out.println((char) b);
        }
        fr.close();
        /*使用字符数组读取*/
        FileReader fr2 = new FileReader("tmp/fr.txt");
        // 定义变量，保存字符
        int len;
        char[] cs = new char[4];
        while ((len = fr2.read(cs)) != -1) {
            System.out.println(new String(cs, 0, len));
        }
        fr2.close();
    }
}
