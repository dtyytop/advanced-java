package com.lzp.java.io;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class FileTest {

    /**
     * 测试分隔符
     */
    @Test
    public void testSeparator() {
        // 路径分隔符 windows：； linux：:
        System.out.println(File.pathSeparator);
        // 目录分隔符 windows：\ linux：/
        System.out.println(File.separator);
    }

    /**
     * 测试构造器
     */
    @Test
    public void testConstructor() throws IOException {
        File file1 = new File("1.properties");
        File file2 = new File("com/main/java/reflect", "refenv.properties");
        File dir = new File("com/main/java/reflect");
        File file3 = new File(dir, "refenv.properties");
        System.out.println(file1);
        System.out.println(file2);
        System.out.println(file3);
        file1.createNewFile();
    }

    /**
     * 获取功能方法
     */
    @Test
    public void testGetMethod() {
        File file = new File("src/main/java/com/lzp");
        System.out.println(file); // toString内部调用getPath()

        // 获取绝对路径
        System.out.println(file.getAbsolutePath());
        // 获取构造方法中传递的路径
        System.out.println(file.getPath());
        // 获取构造方法中传递的路径的末尾部分
        System.out.println(file.getName());
        // 获取构造方法指定文件的大小，以字节为单位
        // 路径不正确，大小为0字节;目录是特殊文件，大小固定为4096字节
        System.out.println(file.length());
    }

    /**
     * 判断方法
     */
    @Test
    public void testIsAndIsnt() {
        File file = new File("src/main/java/com/lzp");
        // 判断路径是否存在
        // 注：此处判断的是系统文件路径，而不是指classpath
        System.out.println(file.exists());
        // 判断是否是文件
        System.out.println(file.isFile());
        // 判断是否是目录
        System.out.println(file.isDirectory());
    }

    /**
     * 创建删除方法
     *
     * @throws IOException
     */
    @Test
    public void testCreateAndRemove() throws IOException {
        // 创建新文件,要求路径必须存在，抛IOException异常
        File newFile = new File("/home/ac/workspace/tmp/1.txt");
        System.out.println(newFile.createNewFile());
        // 创建单级和多级目录
        File mkdirFile = new File("/home/ac/workspace/tmp/lzp");
        System.out.println(mkdirFile.mkdir());
        File mkdirsFile = new File("/home/ac/workspace/tmp/li/zh/p");
        System.out.println(mkdirsFile.mkdirs());
        // 删除文件和目录
        System.out.println(newFile.delete());
        System.out.println(mkdirFile.delete());
        System.out.println(mkdirsFile.delete());
    }

    /**
     * 遍历目录方法
     */
    @Test
    public void testPrintDir() {
        File dir = new File(".");
        for (String f : dir.list()) {
            System.out.println(f);
        }
        System.out.println("-------");
        for (File f : dir.listFiles()) {
            System.out.println(f.getName());
        }
    }

    /**
     * 测试打印所有Java文件
     */
    @Test
    public void testPrintAllJavaFile() {
        FileUtil.printDirLambda(new File("."));
    }

    /**
     * 删除目录以及目录下所有文件
     */
    @Test
    public void testDeleteDir() {
        FileUtil.deleteDir(new File("tmp"));
    }
}
