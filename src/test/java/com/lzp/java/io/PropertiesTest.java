package com.lzp.java.io;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.Properties;
import java.util.Set;

/**
 * 测试使用Properties类
 */
public class PropertiesTest {
    @Before
    public void init() throws IOException {
        File root = new File("tmp");
        root.mkdir();

        /**
         * 文本中的数据，必须是键值对形式，可以使用空格、等号、冒号等符号分隔。
         */
        File file = new File("tmp/test.properties");
        FileOutputStream fos = new FileOutputStream(file);
        fos.write("key1=value1\n".getBytes());
        fos.write("key2:value2".getBytes());
        fos.close();
    }

    @After
    public void destroy() {
        File tmpDir = new File("tmp");
        FileUtil.deleteDir(tmpDir);
        tmpDir.delete();
    }

    /**
     * 测试基本使用
     */
    @Test
    public void testUseOfProperties() {
        Properties properties = new Properties();
        /*设置属性*/
        properties.setProperty("filename", "a.txt");
        properties.setProperty("length", "123");
        /*获取属性*/
        System.out.println(properties);
        System.out.println(properties.getProperty("filename"));
        /*获取key集合*/
        Set<String> keys = properties.stringPropertyNames();
        System.out.println(keys);
    }

    /**
     * 测试与流相关的方法
     */
    @Test
    public void testMethodAboutStream() throws IOException {
        // 创建对象
        Properties properties = new Properties();
        // 获取输入流中的数据
        properties.load(new FileReader("tmp/test.properties"));
        // 遍历获取属性
        System.out.println(properties);
        System.out.println(properties.getProperty("key1"));
    }
}
