package com.lzp.jdbc.jdbctemplate;

import com.lzp.jdbc.datasource.druid.DruidUtil;
import org.junit.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * 测试使用JDBCTemplate
 */
public class JDBCTemplateTest {
    // 创建JdbcTemplate
    private JdbcTemplate template = new JdbcTemplate(DruidUtil.getDataSource());

    /**
     * 测试更新
     */
    @Test
    public void testUpdate() {
        // 定义SQL
        String sql = "update account_jdbc set balance = 4000 where id = ?";
        // 执行sql
        int result = template.update(sql, 3);
        assert result == 1;
    }

    /**
     * 测试新增记录
     */
    @Test
    public void testInsert() {
        String sql = "insert into account_jdbc value(null, ?, ?)";
        int result = template.update(sql, "账户5", "5000.0");
        assert result == 1;
    }

    /**
     * 测试删除
     */
    @Test
    public void testDelete() {
        String sql = "delete from account_jdbc where name = ?";
        int result = template.update(sql, "账户5");
        assert result == 1;
    }

    /**
     * 查询id为1的记录，将其封装为Map
     * 注意：该方法的结果集只能是1
     */
    @Test
    public void testQueryForMap(){
        String sql = "select * from account_jdbc where id = ?";
        Map<String,Object> map = template.queryForMap(sql, 1);
        System.out.println(map);
    }

    /**
     * 将结果集封装成List
     * 注：每一条记录是一个map
     */
    @Test
    public void testQueryForList(){
        String sql = "select * from account_jdbc";
        List<Map<String, Object>> list = template.queryForList(sql);
        System.out.println(list);
    }

    /**
     * 查询所有记录，将其封装成Account对象的List集合
     * --利用RowMapper接口
     */
    @Test
    public void testQueryRowMapper(){
        String sql = "select * from account_jdbc";
        List<Account> list = template.query(sql, new RowMapper<Account>() {
            @Override
            public Account mapRow(ResultSet resultSet, int i) throws SQLException {
                Account account = new Account();
                account.setId(resultSet.getInt("id"));
                account.setName(resultSet.getString("name"));
                account.setBalance(resultSet.getString("balance"));
                return account;
            }
        });
        System.out.println(list);
    }

    /**
     * 查询所有记录，将其封装成Account对象的List集合
     * --利用BeanPropertyRowMapper实现类
     */
    @Test
    public void testQueryBeanPropertyRowMapper(){
        String sql = "select * from account_jdbc";
        List<Account> list = template.query(sql, new BeanPropertyRowMapper<>(Account.class));
        System.out.println(list);
    }

    /**
     * 查询总记录数
     */
    @Test
    public void testQueryForObject(){
        String sql = "select count(id) from account_jdbc";
        Integer result = template.queryForObject(sql, Integer.class);
        assert result == 4;
    }
}
